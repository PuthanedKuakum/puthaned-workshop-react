import * as React from "react";
import { useState } from "react";
import { useEffect } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Link from "@mui/material/Link";
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { Button, ButtonGroup } from "@mui/material";
import Navbar from "src/components/Por/Navbar";

export default function Users() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    UserGet();
  }, []);

  const UserGet = () => {
    console.log("Fetching user data...");
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users`)
      .then((res) => {
        console.log("Response status:", res.status);
        return res.json();
      })
      .then((result) => {
        console.log("User data received:", result);
        setItems(result);
      })
      .catch((error) => {
        console.log("Error fetching user data:", error);
      });
  };

  const UserUpdate = (id) => {
    window.location = `/workShop/kunathip/workshop-userapi/UserUpdate/${id}`;
  };

  const UserDelete = (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
      id: id,
    });

    var requestOptions = {
      method: "DELETE",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    const deleteUrl = `${process.env.NEXT_PUBLIC_API_URL}/users/delete`;

    fetch(deleteUrl, requestOptions)
      .then((response) => {
        console.log("Delete User Response Status:", response.status);

        return response.json();
      })
      .then((result) => {
        console.log("Deleting user with ID:", id);
        console.log("Delete User Result:", result);

        alert(result["message"]);
        if (result["status"] === "ok") UserGet();
      })
      .catch((error) => {
        console.log("Delete User Error:", error);
      });
  };

  return (
    <>
      <Navbar />
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg" sx={{ p: 2 }}>
          <Paper sx={{ p: 2 }}>
            <Box display="flex">
              <Box sx={{ flexGrow: 1 }}>
                <Typography variant="h6" gutterBottom component="div">
                  Users
                </Typography>
              </Box>
              <Box>
                <Link href="UserCreate">
                  <Button className="button" sx={{ backgroundColor: "#19a7ce", color: "white" }}>
                    Create
                  </Button>
                </Link>
              </Box>
            </Box>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>ID</TableCell>
                    <TableCell align="center">Avatar</TableCell>
                    <TableCell align="right">First Name</TableCell>
                    <TableCell align="right">Last Name</TableCell>
                    <TableCell align="right">Username</TableCell>
                    <TableCell align="right">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map((row) => (
                    <TableRow
                      key={row.name}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell component="th" scope="row">
                        {row.id}
                      </TableCell>
                      <TableCell align="center">
                        <Box display="flex" justifyContent="center">
                          <Avatar alt={row.username} src={row.avatar} />
                        </Box>
                      </TableCell>
                      <TableCell align="right">{row.fname}</TableCell>
                      <TableCell align="right">{row.lname}</TableCell>
                      <TableCell align="right">{row.username}</TableCell>
                      <TableCell align="right">
                        <ButtonGroup variant="outlined" aria-label="outlined button group">
                          <Button
                            className="button"
                            sx={{
                              backgroundColor: "#19a7ce",
                              color: "white",
                              borderColor: "white",
                            }}
                            onClick={() => UserUpdate(row.id)}
                          >
                            Edit
                          </Button>
                          <Button
                            className="button"
                            sx={{
                              backgroundColor: "#19a7ce",
                              color: "white",
                              borderColor: "white",
                            }}
                            onClick={() => UserDelete(row.id)}
                          >
                            Delete
                          </Button>
                        </ButtonGroup>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Container>
      </React.Fragment>
    </>
  );
}
