import { Typography } from "@mui/material";
import React from "react";
import Navbar from "src/components/miw/workshop6/navber";
import Workshop6 from "src/components/miw/workshop6/workshop6";
import MainFeaturedPost from "src/components/miw/workshop6/MainFeaturedPost";

const mainFeaturedPost = {
  title: "Title of a longer featured blog post",
  description:
    "Multiple lines of text that form the lede, informing new readers quickly and efficiently about what's most interesting in this post's contents.",
  image: "https://source.unsplash.com/random?wallpapers",
  imageText: "main image description",
  linkText: "Continue reading…",
};
const index = () => {
  return (
    <>
      <Typography sx={{ backgroundColor: "#212121" }}>
        <Navbar />
        <MainFeaturedPost post={mainFeaturedPost} />
        <Typography sx={{ marginY: "2rem" }}>
          <Workshop6 />
        </Typography>
      </Typography>
    </>
  );
};

export default index;
