// pages/calculator.js
import React from "react";
import MyLayout from "src/pages/workShop/tong/MyLayout";
import Calculator from "src/components/Tong/Calculator";

const CalculatorPage = () => {
  return (
    <MyLayout>
      {/* เนื้อหาหน้าคำนวณ */}
      <Calculator />
    </MyLayout>
  );
};

export default CalculatorPage;
