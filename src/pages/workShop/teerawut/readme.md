# Freshy's Workshop

This project is the source of all my workshop during my training in TCC Tecnology Co, Ltd. as a React - Frontend Developer. There will be a total of 6 workshops, all of which are assigned by P'Wu Starting from experimenting with writing a resume using NextJS and MUI. P'Wu allows the use of TailwindCSS, but from the second task onwards, use only the MUI. We will learn logging, commenting and coding best practices, using the API, and finally, making documentation.

## Table of Contents

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Folder Structure](#folder-structure)
- [Development](#development)
  - [Running the Development Server](#running-the-development-server)
  - [Environment Variables](#environment-variables)
- [Build and Deployment](#build-and-deployment)
  - [Building for Production](#building-for-production)
  - [Deployment](#deployment)
- [Code Quality](#code-quality)
  - [Linting](#linting)
- [Workshop](#workshop)
  - [Pre-Workshop](#pre-workshop---workshop-collection)
  - [Workshop 2](#workshop-2---homco-design-ui)
  - [Workshop 3, 4, 5](#workshop-3-4-5---loginapp-melivecode)

## Getting Started

Welcome to **Freshy's Workshop!** This section will guide you through the process of setting up the project on your local machine.

### Prerequisites

Before you begin, make sure you have the following software installed on your machine:

- [Node.js](https://nodejs.org/) (version 18.7.0 or higher)
- [npm](https://www.npmjs.com/) (version 10.2.3 or higher)

### Installation

A step-by-step guide on how to install project dependencies and get the development environment set up.

Follow these steps to get the project up and running:

1. **Clone the repository:**

```bash
git clone https://gitlab.com/H0RosH1/workshop-react.git <your-project-name>
```

2. **Navigate to the project directory:**

```bash
cd your-project
```

3. **Install dependencies:**

```bash
npm install
```

## Folder Structure

Here's an overview of the project's folder structure that I use:

- **`src/pages/workShop/teerawut`**: This directory contains My workshop pages. Each `.js` or `.jsx` file inside this directory becomes a route in your application.

- **`src/components/freshy`**: This directory is for reusable React components used throughout the application.

- **`src/styles`**: This directory holds global styles, theme configurations, or CSS modules.

- **`/public/assets/freshy`**: This directory is for static assets such as images, fonts, or other files that are directly served by Next.js.

## Development

During development, you may need to perform various tasks such as running the development server, handling environment variables, and understanding the project's folder structure.

### Running the Development Server

To start the development server, use the following command:

```bash
npm run dev
```

### Environment Variables

- Dependencies variables in .env.local file

```env
  NEXT_PUBLIC_APP_HOST=https://www.melivecode.com/api
```

Used to access API from melivecode in workshop 3, 4, 5 about JWT-Authentication

- Dependencies variables in next.config.js

```js
module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["mobilephonesdb.com"],
  },
};
```

Used to access images with the Image Component of Next.js that receives images from domain mobilephonesdb.com.

## Build and Deployment

Before deploying your Next.js application, it's essential to understand how to build the project for production and deploy it to a hosting platform.

### Building for Production

To build your project for production, use the following command:

```bash
npm run build
```

This command will generate an optimized production build in the /build or /out directory, depending on your Next.js version. The generated files are ready to be deployed to a hosting service.

### Deployment

There are various hosting platforms available for deploying Next.js applications. Choose a platform that suits your needs and follow its deployment process.

#### Example: Deploying to Vercel

Vercel is a popular platform for deploying Next.js applications. If you haven't already, sign up for a Vercel account.

1. Install the Vercel CLI globally:

```bash
npm install -g vercel
```

2. Log in to your Vercel account using the CLI:

```bash
vercel login
```

3. Navigate to your project's root directory and deploy your application:

```bash
vercel
```

4. Follow the prompts to configure your deployment settings.

Once the deployment process is complete, Vercel will provide you with a unique URL where your application is hosted.

## Code Quality

Maintaining code quality is crucial for the long-term sustainability of your Next.js project. This section covers linting and testing practices.

### Linting

Linting helps catch syntax errors, enforce coding standards, and identify potential issues early in the development process. In this project, we use [ESLint](https://eslint.org/).

#### Running ESLint

To lint your code, run the following command:

```bash
npm run lint
```

# Workshop

Welcome to **Freshy's Workshop!** This section will give information about each workshop in my project.

## Pre-Workshop - Workshop Collection:

Fork the project in gitlab first at **`https://gitlab.com/H0RosH1/workshop-react`**
Create components to store your own workshop, decorate the pages that store your own work as appropriate.

- **`src/pages/teerawut/index.js`**: This file contains My Workshop Collection pages.

## Import List:

- **`src/layouts/dashboard/layout.js`**: This is dashboard template layout.
- **`next/font/google`**: This is next-font from Google font.
- **`@mui/material/Container`**: this is a MUI container component.
- **`src/components/freshy/introduction`**: this is introduction section for my workshop collection.
- **`src/components/freshy/workshop`**: this is workshop tabs section that contain all my workshop link.
- **`src/components/freshy/playground`**: this is playground tabs section that contain all my hobby link.

```js
import { Layout } from "src/layouts/dashboard/layout";
import { Inter } from "next/font/google";
import { Container } from "@mui/material";
import Introduction from "src/components/freshy/introduction";
import Workshop from "src/components/freshy/workshop";
import Playground from "src/components/freshy/playground";
```

## Components

**Introduction Component**

- **`<Introduction/>`**: This component need object props name **`'profile'`** that contain a user profile information to display on a page.

```jsx
<Introduction profile={teerawutProfile} />
```

**Example of Profile Structure**:

```js
const teerawutProfile = {
  nickName: "Freshy",
  fullName: "Teerawut Sungkagaro",
  avatar:
    "https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/avatar-freshy.png?alt=media&token=55cc979e-a0db-4017-816f-a4ae58010725",
  biography: `Bachelor of Information Technology, College of Computing - Khon Kaen University, 
    Thailand. Interesting to know more about programming and explore experiences.`,
  contacts: {
    github: "https://github.com/trwfs00",
    linkedin: "https://www.linkedin.com/in/trwfs00/",
    dribbble: "https://dribbble.com/trwfs0",
  },
};
```

**Workshop Component**

- **`<Workshop/>`**: This component need object props name **`'workshops'`** that contain a my workshop information to display on a page. by it'll show you list of tab's name and when you select any of that tab, it'll show you detail of workshop - { title, subtitle, date, due date, repos link, repos https, thumbnail }. so this component has comments in each paragraph that tell you how it work.

```jsx
<Workshop workshops={workshops} />
```

**Example of Workshops Structure**

```js
const workshops = [
    {
        tabName: 'Pre Workshop',
        title: 'Resume Website',
        subtitle: 'Design your own resume website with beautiful and comment code at least 2 pages.',
        date: '2023-11-15',
        dueDate: '2023-11-15',
        reposLink: 'https://github.com/trwfs00/resume-website',
        reposHTTPS: 'https://github.com/trwfs00/resume-website.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-16%20131231.png?alt=media&token=b652a651-e1fe-42d5-b6cd-f507d9a70530'
    },
    ...
];
```

**Playground Component**

- **`<Playground/>`**: This component need object props name **`'playgrounds'`** that contain a my hobby information to display on a page. by it'll show you list of tab's name and when you select any of that tab, it'll show you detail of hobby - { title, subtitle, date, repos link, repos https, thumbnail }. so this component has comments in each paragraph that tell you how it work.

```jsx
<Playground playgrounds={playgrounds} />
```

**Example of Workshops Structure**

```js
const playgrounds = [
    {
        tabName: "Playground 1",
        title: 'Weather Web Application',
        subtitle: 'Web Application check weather, temperature and air quality.',
        date: '2023-11-17',
        reposLink: 'https://gitlab.com/trwfs00/workshop-react',
        reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-17%20144041.png?alt=media&token=dfb4352b-bd14-4b47-aa29-39aee55a04d9',
        demoHref: '/weatherApp'
    },
    ...
];
```

## Workshop 2 - HOMCO Design UI:

Web design with UI from Figma **`https://www.figma.com/file/JbV1tKPt8CZXQdgp5EghEd/Home-Interior-Design-Website-Wireframe-(Community)?type=design&node-id=0-1&mode=design&t=6SFVcbSrR6SI2WCY-0`** make beautifully decorated, separating the components into sections as appropriate (2 pages+)

- **`src/pages/teerawut/designUI/index.js`**: This file contains the Workshop 2 - HOMECO Design UI

### Import List:

- **`src/components/freshy/designUI/appBar`**: This is responsive navbar component.
- **`src/components/freshy/designUI/hero`**: this is hero section component that tell the main topic in the top of page.
- **`src/components/freshy/designUI/campaign`**: This is campagn section component that tell what our service are.
- **`src/components/freshy/designUI/introduction"`**: this is
- **`src/components/freshy/designUI/procedure"`**: this is
- **`src/components/freshy/designUI/carousel`**: this is
- **`src/components/freshy/designUI/partner`**: this is
- **`src/components/freshy/designUI/trust`**: this is
- **`src/components/freshy/designUI/feedback`**: this is
- **`src/components/freshy/designUI/footer`**: this is

```js
import { Container, Paper } from "@mui/material";
import ResponsiveAppBar from "src/components/freshy/designUI/appBar";
import HeroSection from "src/components/freshy/designUI/hero";
import Campaign from "src/components/freshy/designUI/campaign";
import Introduction from "src/components/freshy/designUI/introduction";
import Procedure from "src/components/freshy/designUI/procedure";
import Carousel from "src/components/freshy/designUI/carousel";
import PartnerSection from "src/components/freshy/designUI/partner";
import TrustSection from "src/components/freshy/designUI/trust";
import Feedback from "src/components/freshy/designUI/feedback";
import FooterSection from "src/components/freshy/designUI/footer";
```

## Components

### 1. ResponsiveAppBar Component

The `ResponsiveAppBar` component is a React component designed for navigation. It provides a responsive app bar with a menu that can be toggled on small screens.

**Overview:**
The component uses Material-UI (`@mui`) components to create a responsive app bar with navigation links. It supports both desktop and mobile views, with a collapsible drawer menu for smaller screens.

**Dependencies:**

- React (`react`)
- Material-UI (`@mui`)

#### Uasge:

```jsx
import ResponsiveAppBar from "./ResponsiveAppBar";

function App() {
  const currentPage = "Home"; // Replace with the current page name or route
  return <ResponsiveAppBar currentPage={currentPage} />;
}
```

#### Props:

**`currentPage`** (string): The name or route of the currently active page.

#### Component Structure

The component consists of the following main parts:

1. Desktop Navbar

- Displays the navigation links in a horizontal layout for larger screens.
- Each link can have a dropdown submenu for pages with children.

2. Mobile Navbar

- Toggled by a menu icon for smaller screens.
- Displays a drawer with a vertical list of links.

#### Component Logic:

- State Variables:

  - **`anchorElSubMenu`**: Manages the anchor element for the desktop submenu.
  - **`anchorElSubMenuMobile`**: Manages the anchor element for the mobile submenu.
  - **`activePage`**: Keeps track of the currently active page.
  - **`openDrawer`**: Manages the visibility of the mobile drawer.

- Methods:

  - **`handleOpenSubMenu`**: Opens the desktop submenu for a specific page.
  - **`handleCloseSubMenu`**: Closes the desktop submenu.
  - **`handleOpenSubMenuMobile`**: Toggles the mobile submenu for a specific page.
  - **`handleCloseSubMenuMobile`**: Closes the mobile submenu.
  - **`useEffect`**: Logs the active page when it changes.

- Styling

  - The component uses Material-UI components for styling, including `AppBar`, `Toolbar`, `IconButton`, `Typography`, `Button`, `Menu`, `Drawer`, `List`, `ListItem`, `ListItemText`, `Paper`, and `Link`.
  - Styling is applied using the sx prop with custom styles for various elements.
  - Colors, fonts, and spacing are configured for a clean and visually appealing design.

- Theming

  - The component uses Material-UI icons for visual elements.
  - The theme is consistent with the specified color palette, font family, and spacing.

- Customization
  - Developers can customize the component by adjusting the pages array, changing the base URL (`'baseUrl'`), or modifying the styling in the `'sx'` props.

### 2. HeroSection Component

**Overview:**
The HeroSection component is a React component that represents the hero section of a webpage. It includes a welcome message, a title, a subtitle, and a button to get started. The hero section is designed to be visually appealing with an accompanying image.

#### Uasge:

```jsx
import HeroSection from "./HeroSection";

function App() {
  return <HeroSection />;
}
```

#### Component Structure:

The `'HeroSection'` component is composed of the following main parts:

1. Container (`'Box'`):

- Flex container with a responsive layout.
- Displays its children in a column-reverse order on small screens (`'xs'`) and in a row order on medium screens (`'md'`).
- Justifies content to the center on small screens and to space-between on medium screens.
- Aligns items to the center.
- Uses the 'Inter' sans-serif font family.
- Adds bottom margin (`'mb'`) of 6 on small screens and 0 on medium screens.

2. Left Section (`'Box'`):

- Flex item with flex of 1.
- Text is aligned to the center on small screens and to the left on medium screens.
- Includes a welcome message, a title, a subtitle, and a button.
- Font sizes of the title vary based on the screen size.

3. Welcome Message (`'Typography'`):

- Overline variant displaying "WELCOME TO HOMCO."

4. Title (`'Typography'`):

- Heading 1 variant displaying "BUILD YOUR ELEGANT DREAM HOME INTERIOR."
- Font size varies responsively.

5. Subtitle (`'Typography'`):

- Subtitle 1 variant displaying a description of the interior design services.

6. Button (`'Button'`):

- Contained variant with custom styling.
- Background color changes on hover.

7. Right Section (`'Box'`):

- Flex item with flex of 1.
- Contains an image using the Next.js Image component.
- The image is imported from the public directory.

#### Styling:

- Custom styling is applied using the Material-UI `'sx'` prop.
- The button has a specific color and hover effect to enhance visual appeal.
- The `'Image'` component is used for a responsive and optimized image display.

#### Example:

```jsx
<HeroSection />
```

### 3. Campaign Component

**Overview:**
The `'Campaign'` component is a React component that represents a section of a webpage containing campaign information. It is designed to display multiple campaigns side by side, each with a title, description, and a button to navigate to more details.

#### Uasge:

```jsx
import Campaign from "./Campaign";

function App() {
  return <Campaign />;
}
```

#### Component Structure:

The `'Campaign'` component is composed of the following main parts:

1. Paper Container (`'Paper'`):

- A Material-UI Paper component that acts as the container for the entire campaign section.
- Flex container with a responsive layout.
- Displays its children in a column order on small screens (`'xs'`) and in a row order on medium screens (`'md'`).
- Justifies content to the center.
- Aligns items to the center on small screens and to the baseline on medium screens.
- Uses the 'Inter' sans-serif font family.
- Adds bottom margin (`'mb'`) of 8.

2. Campaign Boxes (`'Box'`):

- Three Box components, each representing a campaign.
- Each Box has a flex display, a flex direction of column, a gap between elements, a background color, padding, a specified width for responsiveness, a maximum height, and a box shadow.
- The content includes a title (`'Typography'`), a description (`'Typography'`), and a button (`'Button'`).
- The button has custom styling for a block display, left alignment, and zero horizontal padding.

3. Campaign Titles (`'Typography'`):

- Heading 5 variant with semibold font weight.
- Displays the title of each campaign.

4. Campaign Descriptions (`'Typography'`):

- Body 1 variant.
- Displays a description of each campaign.

5. Campaign Buttons (Button):

- Text variant with custom styling.
- Displays an arrow (`'-->'`) as a link to more details.

#### Styling:

- Custom styling is applied using the Material-UI sx prop.
- Each campaign box has a unique background color, creating visual distinction.
- The button styling includes text decoration, font weight, color, display, text alignment, and padding.

#### Example:

```jsx
<Campaign />
```

### 4. Introduction Component

**Overview:**
The `'Introduction'` component is a React component designed to introduce and provide key information about a home interior decoration team. It includes an image, introductory text, a checklist of key features, and insights into the team's experience and achievements.

#### Usage:

```jsx
import Introduction from "./Introduction";

function App() {
  return <Introduction />;
}
```

#### Component Structure:

The `'Introduction'` component is composed of the following main parts:

1. Paper Container (Paper):

- A Material-UI Paper component that acts as the container for the entire introduction section.
- Flex container with a responsive layout.
- Displays its children in a column order on small screens (xs) and in a row order on medium screens (md).
- Justifies content to the center.
- Uses the 'Inter' sans-serif font family.
- Adds margin (my) of 12 on small screens and 24 on medium screens.
- Includes a gap of 4 between elements.

2. Left Section (Box):

- Flex item with a flex of 1.
- Contains an image using the Next.js Image component.
- The image is imported from the public directory.
- Styles include object-fit: cover, border-radius: 12 for a visually appealing display.

3. Right Section (Box):

- Flex item with a flex of 1.
- Includes various text elements and grids for a structured layout.
- Typography components for overline, heading, and subtitle.
- Responsive font size for the title.
- Two grids to organize the checklist and insights.
- Checklist items (CheckListItem): A list of checkable features with corresponding icons.
- Insights items (InsightItem): A list of insights into the team's experience and achievements.

4. Checklist (List):

- Two lists, each containing three checklist items.
- Each checklist item is represented by the CheckListItem component.
- Items include a start icon (CheckCircleIcon), a primary title, and a custom icon color.

5.  Insights (Grid):

- A grid containing insight items (InsightItem).
- Each insight item displays a title and a subtitle.

#### Component Logic:

The `'Introduction'` component incorporates minimal logic, primarily focused on rendering dynamic content. The logic involves the following:

1. Imported Components:

- The component imports necessary Material-UI components, icons, and custom components (`'CheckListItem'` and `'InsightItem'`).

2. Static Data:

- The `'introductionObject'` object contains static data for the introduction section, such as introductory text, titles, subtitles, checklist items, and insights.

```jsx
const introductionObject = {
  intro: "WHO ARE WE",
  title: "WE ARE PERFECT TEAM FOR HOME INTERIOR DECORATION",
  subtitle: `Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, 
      totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
      Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, 
      sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.`,
  checkList: [
    { title: "Flexible Time", startIcon: <CheckCircleIcon /> },
    { title: "Perfect Work", startIcon: <CheckCircleIcon /> },
    { title: "Client Priority", startIcon: <CheckCircleIcon /> },
    { title: "Flexible Time", startIcon: <CheckCircleIcon /> },
    { title: "Perfect Work", startIcon: <CheckCircleIcon /> },
    { title: "Client Priority", startIcon: <CheckCircleIcon /> },
  ],
  insight: [
    { title: "15Y", subtitle: "EXPERIENCE" },
    { title: "25+", subtitle: "BEST TEAM" },
    { title: "500+", subtitle: "TOTAL CLIENT" },
  ],
};
```

3. Rendering Dynamic Content:

- The component dynamically renders content based on the data in the `'introductionObject'`.

4. Checklist and Insights:

- Two grids organize the checklist and insights, respectively.
- Checklist items are mapped from the `'introductionObject.checkList'` array and rendered using the `'CheckListItem'` component.
- Insight items are mapped from the `'introductionObject.insight'` array and rendered using the `'InsightItem'` component.

5. Component Composition:

- The `'Introduction'` component is composed of various Material-UI components, creating a visually appealing and structured introduction section.

The `'CheckListItem'` component is a React component designed to represent a checklist item within a ListItem. It includes a start icon, a primary text, and customizable icon and text colors.

```jsx
import CheckListItem from "./CheckListItem";

function App() {
  const itemData = {
    startIcon: <CheckCircleIcon />,
    primary: "Flexible Time",
    iconColor: "#3e99ec",
    textColor: "black",
  };
  return (
    <CheckListItem
      startIcon={itemData.startIcon}
      primary={itemData.primary}
      iconColor={itemData.iconColor}
      textColor={itemData.textColor}
    />
  );
}
```

The `'InsightItem'` component is a simple React component designed to display insights in a grid layout. It includes a title and a subtitle, both of which are dynamically provided as props.

```jsx
import InsightItem from "./InsightItem";

function App() {
  const itemData = { title: "15Y", subtitle: "EXPERIENCE" };
  return <InsightItem title={itemData.title} subtitle={itemData.subtitle} />;
}
```

#### Styling:

- Custom styling is applied using the Material-UI sx prop.
- The image has a border-radius for a rounded appearance.
- Typography components have responsive styling for font size.
- Checklists and insights are organized using grids.

#### Example:

```jsx
<Introduction />
```

### 5. Procedure Component

**Overview:**
The `'Procedure'` component is a React component designed to showcase and describe the steps involved in the working procedure of a design-related service. It includes three sections, each representing a step in the procedure, with icons, titles, and descriptions.

#### Usage:

```jsx
import Procedure from "./Procedure";

function App() {
  return <Procedure />;
}
```

#### Component Structure:

The `'Procedure'` component consists of the following parts:

1. Paper Container (`'Paper'`):

- A Material-UI Paper component that serves as the overall container for the procedure sections.
- Custom styling is applied for spacing and layout using the `'sx'` prop.

2. Title and Overline (`'Typography'`):

- Two Typography components that display the overline and main title of the procedure.
- The title font size is responsive, adapting to different screen sizes.

3. Procedure Sections (`'Box'`):

- Three sections represented by Box components, each containing:
- An icon related to the step.
- Title text indicating the step.
- Description text explaining the step.
- The sections are displayed in a row on medium screens (`'md'`) and in a column on small screens (`'xs'`).

#### Styling:

- Custom styling is applied using the Material-UI sx prop.
- Each section has a background color, padding, width, and box shadow for a visually appealing design.
- The icons and text are styled for consistency and readability.

#### Example:

```jsx
<Procedure />
```

### 6. Procedure Component

**Overview:**
The `'Carousel'` component is a React component designed to showcase a series of slides in a horizontally scrollable manner. It utilizes the Material-UI Paper and Slide components to create an engaging carousel effect. The slides are sourced from an array of objects containing titles and image URLs.

#### Usage:

```jsx
import Carousel from "./Carousel";

function App() {
  return <Carousel />;
}
```

#### Component Structure:

The `'Carousel'` component consists of the following parts:

1. Slide Container (`'Box'`):

- A Material-UI `'Box'` component serving as the container for the slides.
- Custom styling is applied for flex display, horizontal scrolling, and removal of the scrollbar.
- The slides are repeated to create an infinite loop effect.

2. Slides (`'Slide'`, `'Paper'`):

- Material-UI `'Slide'` components are used to control the entrance and exit animations of the slides.
- Material-UI `'Paper'` components represent each slide, containing an image fetched from the provided URLs.
- Custom styling is applied for centering and responsiveness.

The `'slides'` array is structured as follows:

```jsx
const slides = [
  {
    title: "Slide 1",
    image:
      "https://img.freepik.com/free-photo/stylish-scandinavian-living-room-with-design-mint-sofa-furnitures-mock-up-poster-map-plants-eleg_1258-152155.jpg?w=1380&t=st=1700541245~exp=1700541845~hmac=4321d3a5661939c4393403c1ab4cc42ebe6efae79a270a8e7e0cdd83e34e417a",
  },
  // ... additional slides
];
```

#### Slide Object Properties

1. title (`'string'`):

- Represents the title of the slide.
- Provides a brief description or identifier for the content of the slide.

2. image (`'string'`):

- Contains the URL of the image associated with the slide.
- The image serves as the visual content of the slide within the carousel.

#### Styling:

- Custom styling is applied using the Material-UI `'sx'` prop.
- Flex display is used for horizontal scrolling.
- The `'scrollSnapType'` property is applied to create a snapping effect during scrolling.
- CSS transformations are applied for a skewed appearance.

#### Example:

```jsx
<Carousel />
```

### Partner Section Component

**Overview:**
The `'PartnerSection'` component is designed to showcase the perfect partners or collaborators. It features a responsive layout with text content and images of partner logos. The component creates a visually appealing presentation with a skew transformation and interactive hover effects.

#### Usage:

The PartnerSection component is meant to be used within a larger application to highlight and promote partnerships or collaborations.

#### Structure

The component is structured as a two-column layout with text on one side and images of partners on the other. The partner images have interactive hover effects, providing a dynamic and engaging user experience.

#### Content

Text Content

- Title (Typography):

  - Represents the main title of the section, indicating the importance of having perfect partners.

- Headline (Typography):

  - Displays a compelling headline that emphasizes the priority of creating dream home designs.

- Description (Typography):

  - Provides a brief description of the partner section, encouraging engagement.

- Button (Button):
  - A call-to-action button linking to the portfolio or relevant section.

Partner Images

- Images (Image):
  - Displays partner logos/images.
  - Images have interactive hover effects, including skew transformations and width adjustments for a dynamic appearance.

#### Example

```jsx
import PartnerSection from "src/components/freshy/designUI/partner";

<PartnerSection />;
```

#### 7. TrustSection Component

**Overview:**
The `'TrustSection'` component is a React functional component that represents a section of a user interface, specifically designed for displaying information related to building trust in home design interior services. It combines a video component (`'TrustVideo'`) with textual content, including a title, subtitle, and a checklist of trust-related attributes.

#### Usage:

```jsx
import TrustSection from "src/components/freshy/designUI/trust.js";

// Inside a parent component or JSX structure
<TrustSection />;
```

#### Component Structure:

- **`Paper`**: A Material-UI Paper component serving as the outer container.

  - Properties:
    - `'elevation={0}'`: No elevation for the paper.
    - `'sx'`: Styling using the Material-UI styling system.

- Box (`'Video Section'`):
  - Properties:
    - `'flex={1}'`: Flex property for responsiveness.
    - `'sx'`: Styling using the Material-UI styling system.
  - Contains TrustVideo component.
- Box (Text Section):
  - Properties:
    - `'flex={1}'`: Flex property for responsiveness.
  - Contains Typography components for introductory text, title, and subtitle.
  - Contains a Grid with two List components, each displaying a subset of trust-related checklist items.

#### Styling:

- Paper Styling:
  - `'display'`: Flex display for layout.
  - `'justifyContent'`: Centered content.
  - `'fontFamily'`: Font styling using Inter and sans-serif.
  - `'flexDirection'`: Responsive flex direction for column layout on small screens and row layout on medium screens.
  - `'my'`: Margin on the y-axis for spacing.
  - `'gap'`: Spacing between children elements.

#### Example:

```jsx
import TrustSection from "src/components/freshy/designUI/trust.js";

// Inside a parent component or JSX structure
<TrustSection />;
```

### 8. Feedback Component

**Overview:**
The `Feedback` component is a React functional component designed to showcase client feedback on a webpage. It includes a paper container with two main sections: the left section containing a title, subtitle, and introductory text, and the right section displaying a star rating, client testimonial, and information about the client.

#### Usage:

```jsx
import Feedback from "src/components/freshy/designUI/feedback.js";

// Inside a parent component or JSX structure
<Feedback />;
```

#### Component Structure:

1. **Paper:**

   - Material-UI Paper component serving as the outer container.
   - Properties:
     - `elevation={0}`: No elevation for the paper.
     - `sx`: Styling using the Material-UI styling system.

2. **Left Section (`Box`):**

   - Flex container with a flex of 1.
   - Contains `Typography` components for the title, subtitle, and introductory text.

3. **Title (`Typography`):**

   - Overline variant displaying "Clients Feedback."

4. **Title (`Typography`):**

   - Heading 1 variant displaying "OUR TESTIMONIAL FROM BEST CLIENTS."
   - Font size varies responsively.

5. **Subtitle (`Typography`):**

   - Subtitle 1 variant displaying a general description of client testimonials.

6. **Right Section (`Box`):**

   - Flex container with a flex of 1.
   - Contains star rating icons, testimonial text, and client information.

7. **Star Rating (`Box`):**

   - Displays a star rating using `StarRateRoundedIcon` and `StarHalfRoundedIcon`.
   - Styled with custom colors and sizing.

8. **Testimonial Text (`Typography`):**

   - Body 1 variant displaying the client's testimonial.

9. **Client Information (`Box`):**
   - Flex container with an avatar and client details.
   - `Avatar` component displays the client's avatar image.
   - Client name and role displayed using `Typography`.

#### Styling:

- Styling applied using the Material-UI `'sx'` prop.
- Custom styling for layout, spacing, and borders.
- Star rating, avatar, and button have specific colors and hover effects.

#### Example:

```jsx
import Feedback from "src/components/freshy/designUI/feedback.js";

// Inside a parent component or JSX structure
<Feedback />;
```

### 3. FooterSection Component

**Overview:**
The `FooterSection` component is a React functional component that represents the footer section of a webpage. It includes information about the company, navigation links, and contact details. The component is designed within a Material-UI Paper container with a blue background.

#### Usage:

```jsx
import FooterSection from "src/components/freshy/designUI/footer.js";

// Inside a parent component or JSX structure
<FooterSection />;
```

#### Component Structure:

1. **Paper:**

   - Material-UI Paper component serving as the outer container.
   - Properties:
     - `elevation={0}`: No elevation for the paper.
     - `square`: Square shape for the paper.
     - `sx`: Styling using the Material-UI styling system.

2. **Container:**

   - Material-UI Container component with a maximum width of 'xl'.
   - Properties:
     - `sx`: Styling using the Material-UI styling system.
     - `position: 'relative'`: Relative positioning.

3. **Contact Component:**

   - Custom `Contact` component (assuming it's a React component).

4. **Grid Container:**

   - Material-UI Grid container to organize child items.
   - Properties:
     - `container`: Specifies a container grid.
     - `spacing={4}`: Spacing between grid items.
     - `direction="row"`: Direction of the container (row).
     - `justifyContent="center"`: Centered content along the horizontal axis.
     - `alignItems="flex-start"`: Items aligned at the start along the vertical axis.
     - `alignContent="stretch"`: Content stretched along the vertical axis.
     - `wrap="wrap"`: Items wrap to the next line when they exceed the container's width.

5. **Grid Item (Information Section):**

   - Grid item taking 12 columns on extra-small screens and 6 columns on medium screens.
   - Contains `Typography` components for the title and introductory text.
   - Social media icons (`FacebookIcon`, `TwitterIcon`, `InstagramIcon`, `LinkedInIcon`) displayed with custom styling.

6. **Grid Item (Navigation Section):**

   - Grid item taking 6 columns on extra-small screens and 3 columns on medium screens.
   - Contains `Typography` component for the title and a `List` component with `LinkList` items.
   - Each `LinkList` item represents a navigation link.

7. **Grid Item (Contact Us Section):**
   - Grid item taking 6 columns on extra-small screens and 3 columns on medium screens.
   - Contains `Typography` component for the title, a `List` component with `CheckListItem` items representing contact information.
   - Includes a `TextField` for entering an email address and a `Button` for subscription.

#### Styling:

- Styling applied using the Material-UI `'sx'` prop.
- Custom styling for the paper background color, padding, and spacing.
- Custom styling for grid layout, typography, icons, and form elements.

#### Example:

```jsx
import FooterSection from "src/components/freshy/designUI/footer.js";

// Inside a parent component or JSX structure
<FooterSection />;
```

## Workshop 3, 4, 5 - LoginApp (Melivecode):

**Workshop 3:** Make a login system using the api **`https://www.melivecode.com/api/login`**

```json
// The information that must be entered is
{
  "username": "karn.yong@melivecode.com",
  "password": "melivecode"
}
```

Show log of login system operation and create .env file.
The log will have info request error.
and store jwt into localstorage

**Workshop 4:**
Use JWT from login. What I did was pull data from this API **`https://www.melivecode.com/api/auth/user`**
Bring data to display and store in localstorage with a log like Workshop 3.

**Workshop 5:**
Make every API use a button to use each API. Show log like past workshops
\*It's like pressing a button to call a function that calls each API line.

**`src/pages/teerawut/loginApp/index.js`**: This file contains the Workshop 3, 4, 5 - LoginApp (Melivecode)

## Import List

- `@mui/material`: Material-UI components for styling.
- `react`: The React library for building user interfaces.
- `src/components/freshy/loginApp/loginPage`: Responsive login page component.
- `src/components/freshy/loginApp/loginPage2`: Alternate login page component with layout wrapper.
- `src/components/freshy/loginApp/mainSection`: Main section component for authenticated users.

```javascript
import { Alert, Snackbar } from "@mui/material";
import React, { useState } from "react";
import LoginPage from "src/components/freshy/loginApp/loginPage";
import LoginPage2, { LayoutLogin } from "src/components/freshy/loginApp/loginPage2";
import MainSection from "src/components/freshy/loginApp/mainSection";
```

## Index.js

### Overview

The `Index.js` file serves as the entry point for the React application. It contains the main component named `Index`, which is responsible for rendering different sections based on the user's authentication status.

### Components

1. **MainSection**: Renders the main content for authenticated users, including user details, options, and tabs.
   - **Path**: `src/components/freshy/loginApp/mainSection`
2. **LayoutLogin**: Provides a layout for the login page, containing a logo and login components.
   - **Path**: `src/components/freshy/loginApp/loginPage2`
3. **Snackbar**: Displays alert messages at the bottom of the screen.
   - **Path**: MUI (Material-UI) component

### State Variables

- **user**: Stores user information when authenticated.
- **openSnackbar**: Controls the visibility of the Snackbar component.
- **snackbarMessage**: Contains the message and status for Snackbar.
- **loading**: Indicates whether a loading state is active.

### Functions

- **handleUser(userData)**: Updates the user state when a user logs in.
- **handleLogout()**: Initiates the logout process, clearing user data and showing a success message.
- **handleCloseSnackbar()**: Closes the Snackbar component.

### useEffect

The `Index` component utilizes the `useEffect` hook to check if the user is already logged in with a valid access token. If a valid access token is found in the local storage, the `fetchUserData` function is called to fetch user details.

Example:

```jsx
useEffect(() => {
  // Check if the user is already logged in with a valid access token
  const accessToken = localStorage.getItem("accessToken");
  if (accessToken) {
    fetchUserData(accessToken);
    console.info(
      `[INFO] ${userData.fname} ${userData.lname} is already logged in with a valid access token`
    );
  }
}, []);
```

### Render

The component conditionally renders either the `MainSection` for authenticated users or the login components for non-authenticated users. It also displays a Snackbar for notifications.

## LoginPage2.js

### Overview

The `LoginPage2.js` file defines the login page component (`LoginPage2`) responsible for user authentication using email and password. It utilizes MUI components for the form and styling.

### State Variables

- **method**: Tracks the authentication method (email or phone number).
- **loading**: Indicates whether a loading state is active.

### Functions

- **handleMethodChange(event, value)**: Updates the authentication method.
- **formik**: Handles form state, validation, and submission.
- **handleSkip()**: Handles the process of skipping authentication.

### useEffect

- The **`'LoginPage2'`** component uses the useEffect hook to fetch user data when the component mounts. This is used to check if the user is already logged in with a valid access token.

```jsx
useEffect(() => {
  // Check if the user is already logged in with a valid access token
  const accessToken = localStorage.getItem("accessToken");
  if (accessToken) {
    fetchUserData(accessToken);
    console.info("[INFO] Fetch userData successfully");
  }
}, []);
```

### Render

Renders a form with email and password fields, allowing users to log in. It also provides a skip authentication option and displays a demo user's information for testing.

## MainSection.js

### Overview

The `MainSection.js` file defines the main content for authenticated users, displaying user details, tabs, and options for different API calls.

### Components

1. **UserList**: Displays a list of users.

- **Path**: `src/components/freshy/loginApp/userList`

2. **UserSearch**: Provides a search functionality for users.
   - **Path**: `src/components/freshy/loginApp/userSearch`
3. **UserPagination**: Handles user pagination.
   - **Path**: `src/components/freshy/loginApp/userPagination`
4. **UserSort**: Allows sorting of users.
   - **Path**: `src/components/freshy/loginApp/userSort`
5. **UserSearchPaginationSort**: Combines search, pagination, and sorting.
   - **Path**: `src/components/freshy/loginApp/userSearchPaginationSort`
6. **UserDetail**: Shows detailed information about a user.
   - **Path**: `src/components/freshy/loginApp/userDetail`
7. **SignupComponent**: Allows user registration.
   - **Path**: `src/components/freshy/loginApp/signupComponent`
8. **UserUpdate**: Handles user profile updates.
   - **Path**: `src/components/freshy/loginApp/userUpdate`
9. **UserDelete**: Manages user deletion.
   - **Path**: `src/components/freshy/loginApp/userDelete`

### State Variables

- **tabValue**: Tracks the selected tab.
- **apiValue**: Tracks the selected API within a tab.
- **openSidebar**: Controls the visibility of the sidebar.

### Functions

- **handleTabChange(event, newValue)**: Updates the selected tab.
- **handleSidebar(newValue)**: Toggles the sidebar for the selected API.
- **switchApi(tab, api)**: Renders the appropriate component based on the selected tab and API.

### useEffect

The **`'MainSection'`** component utilizes the useEffect hook to log changes in the selected tab and API values.

```jsx
useEffect(() => {
  console.log("tabs:", tabValue);
  console.log("api", apiValue);
}, [apiValue, tabValue]);
```

### Render

Displays user details, tabs, and a sidebar with options for different API calls based on the selected tab.

## UserList Component Documentation

### Overview

The `UserList` component is responsible for rendering a table displaying a list of users. It fetches user data from the server and displays it in a tabular format. The component includes features to handle loading states and presents a loading spinner while the data is being fetched.

### Component Structure

The component is structured as follows:

- **Dependencies**
  - `@mui/material`: Imports necessary Material-UI components for styling and layout.
  - `react`: Imports the React library.
- **State Variables**

  - `data`: Manages the user data fetched from the server.
  - `loading`: Indicates whether the data is still being fetched.

- **useEffect**
  - The `useEffect` hook is utilized to fetch user data from the server when the component mounts. It triggers the `fetchData` function, which performs an asynchronous API call using the `fetch` function. The fetched data is then stored in the `data` state.

#### Example:

```jsx
useEffect(() => {
  const fetchData = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/users`);
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      const result = await response.json();

      console.info("[INFO] Fetched user list successfully");
      console.log("UserList", result);
      setData(result);
    } catch (error) {
      console.error("Error fetching data:", error);
    } finally {
      setLoading(false);
    }
  };

  fetchData();
}, []);
```

## Render

- The `TableContainer`, `Table`, `TableHead`, and `TableBody` components from Material-UI are used to structure the table.
- Conditional rendering is implemented to display a loading spinner while the data is being fetched.

#### Example:

```jsx
return (
  <TableContainer component={Paper} sx={{ my: 2 }}>
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>ID</TableCell>
          <TableCell>Avatar</TableCell>
          <TableCell>First Name</TableCell>
          <TableCell>Last Name</TableCell>
          <TableCell>Username</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {loading ? (
          <TableRow>
            <TableCell colSpan={5} rowSpan={5}>
              <CircularProgress />
            </TableCell>
          </TableRow>
        ) : (
          data.map((user) => <TableRow key={user.id}>{/* User data cells */}</TableRow>)
        )}
      </TableBody>
    </Table>
  </TableContainer>
);
```

#### Example Usage:

```jsx
import UserList from "src/components/freshy/loginApp/userList";

const ExampleComponent = () => {
  return <UserList />;
};
```

This component can be imported and used in other parts of the application where a user list needs to be displayed.

## UserSearch Component Documentation

### Overview

The `UserSearch` component is responsible for providing a search functionality to find users based on a specified query. It consists of a search input field, a search button, and a table to display the search results. The component uses state variables to manage the search query, fetched data, and loading states.

### Component Structure

The component is structured as follows:

- **Dependencies**

  - `@mui/material`: Imports necessary Material-UI components for styling and layout.
  - `@mui/system`: Imports the `Box` component from the MUI system.
  - `react`: Imports the React library.

- **State Variables**

  - `searchQuery`: Manages the input value of the search query.
  - `data`: Manages the user data fetched from the server based on the search query.
  - `loading`: Indicates whether the data is still being fetched.

- **Functions**
  - `handleSearch`: Performs an asynchronous API call to fetch user data based on the current search query.
  - `handleKeyPress`: Listens for the "Enter" key press in the search input and triggers the search if pressed.

#### Example:

```jsx
const handleSearch = async () => {
  setLoading(true);

  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/users?search=${searchQuery}`);
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const result = await response.json();
    console.info("[INFO] Searched user successfully");
    console.log("UserSearch", result);
    setData(result);
  } catch (error) {
    console.error("Error fetching data:", error);
  } finally {
    setLoading(false);
  }
};

const handleKeyPress = (e) => {
  if (e.key === "Enter") {
    handleSearch();
  }
};
```

### Render

- The `TextField` and Button components from Material-UI are used for the search input and search button, respectively.
- The `TableContainer`, `Table`, `TableHead`, and `TableBody` components are used to structure the table for displaying search results.
- Conditional rendering is implemented to display a loading message while the data is being fetched.

#### Example:

```jsx
return (
    <>
        <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItem: 'center', gap: 2 }}>
            {/* Search input and button */}
            <TextField
                label="Search"
                variant="outlined"
                value={searchQuery}
                fullWidth
                onChange={(e) => setSearchQuery(e.target.value)}
                onKeyDown={handleKeyPress}
            />
            <Button variant="contained" color="primary" onClick={handleSearch}>
                Search
            </Button>
        </Box>

        {/* Search results table */}
        <TableContainer component={Paper} style={{ marginTop: 20 }}>
            <Table>
                <TableHead>
                    <TableRow>
                        {/* Table headers */}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {loading ? (
                        <TableRow>
                            <TableCell colSpan={5}>Loading...</TableCell>
                        </TableRow>
                    ) : (
                        // User data cells
                    )}
                </TableBody>
            </Table>
        </TableContainer>
    </>
);
```

#### Example Usage:

```jsx
import UserSearch from "src/components/freshy/loginApp/userSearch";

const ExampleComponent = () => {
  return <UserSearch />;
};
```

This component can be imported and used in other parts of the application where a user search functionality is required.

## UserPagination Component Documentation

### Overview

The `UserPagination` component provides a paginated view of user data. It includes a table to display user information, a dropdown for selecting the number of records per page, and a pagination control for navigating through pages. The component utilizes state variables to manage the pagination-related information, such as the current page, records per page, total pages, and the fetched user data.

### Component Structure

The component is structured as follows:

- **Dependencies**

  - `@mui/material`: Imports necessary Material-UI components for styling and layout.
  - `@mui/system`: Imports the `Box` component from the MUI system.
  - `react`: Imports the React library.

- **State Variables**

  - `perPage`: Manages the number of records to display per page.
  - `page`: Manages the current page being displayed.
  - `totalPages`: Represents the total number of pages available.
  - `data`: Manages the user data fetched based on the current pagination settings.
  - `loading`: Indicates whether the data is still being fetched.

- **Functions**
  - `handlePageChange`: Handles the change in the current page when the user interacts with the pagination control.
  - `handlePerPageChange`: Handles the change in the number of records per page when the user selects a new value from the dropdown.
  - `handleFetchData`: Performs an asynchronous API call to fetch user data based on the current pagination settings.

#### Example:

```jsx
const handlePageChange = (event, value) => {
  setPage(value);
};

const handlePerPageChange = (event) => {
  setPerPage(event.target.value);
};

const handleFetchData = async () => {
  setLoading(true);

  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_APP_HOST}/users?page=${page}&per_page=${perPage}`
    );
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const result = await response.json();

    console.info("[INFO] Fetched user pagination successfully");
    console.log("UserPagination", result);
    setData(result.data);
    setTotalPages(result.total_pages);
  } catch (error) {
    console.error("Error fetching data:", error);
  } finally {
    setLoading(false);
  }
};

useEffect(() => {
  handleFetchData();
}, [page, perPage]);
```

### Render

- The TableContainer, Table, TableHead, and TableBody components from Material-UI are used to structure the table for displaying paginated user data.
  The Select component is used for the dropdown to select the number of records per page.
- The Pagination component is used for navigating through different pages of user data.
  Conditional rendering is implemented to display a loading message while the data is being fetched.

#### Example:

```jsx
return (
  <>
    {/* User data table */}
    <TableContainer component={Paper} style={{ marginTop: 20 }}>
      <Table>{/* Table structure */}</Table>
    </TableContainer>

    {/* Pagination and records per page control */}
    <Box
      sx={{ display: "flex", justifyContent: "space-between", alignItems: "center", mt: 2, gap: 2 }}
    >
      {/* Records per page dropdown */}
      <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center", gap: 2 }}>
        <Typography variant="button">Row per page:</Typography>
        <Select value={perPage} onChange={handlePerPageChange} label="Records Per Page">
          <MenuItem value={5}>5</MenuItem>
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={20}>20</MenuItem>
        </Select>
      </Box>

      {/* Pagination control */}
      <Pagination
        count={totalPages}
        page={page}
        onChange={handlePageChange}
        color="primary"
        style={{ marginTop: 20, display: "flex", justifyContent: "center" }}
      />
    </Box>
  </>
);
```

#### Example Usage:

```jsx
import UserPagination from "src/components/freshy/loginApp/userPagination";

const ExampleComponent = () => {
  return <UserPagination />;
};
```

This component can be imported and used in other parts of the application where paginated user data needs to be displayed.

## UserSort Component Documentation

### Overview

The `UserSort` component is designed to display a paginated table of user data with the ability to sort the data based on different columns and in ascending or descending order. It includes dropdowns for selecting the sort column and order, and it fetches the sorted data accordingly.

### Component Structure

The component is structured as follows:

- **Dependencies**

  - `@mui/material`: Imports necessary Material-UI components for styling and layout.
  - `react`: Imports the React library.

- **State Variables**

  - `data`: Manages the user data fetched from the API based on the selected sort settings.
  - `loading`: Indicates whether the data is still being fetched.
  - `sortColumn`: Manages the currently selected column for sorting.
  - `sortOrder`: Manages the selected order for sorting (ascending or descending).

- **Functions**
  - `fetchData`: Performs an asynchronous API call to fetch user data based on the current sort settings.
  - `handleSortColumnChange`: Handles the change in the selected sort column from the dropdown.
  - `handleSortOrderChange`: Handles the change in the selected sort order from the dropdown.

#### Example:

```jsx
const fetchData = async () => {
  setLoading(true);

  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_APP_HOST}/users?sort_column=${sortColumn}&sort_order=${sortOrder}`
    );
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const result = await response.json();

    console.info("[INFO] Fetched user sort successfully");
    console.log("UserSort", result);
    setData(result);
  } catch (error) {
    console.error("Error fetching data:", error);
  } finally {
    setLoading(false);
  }
};

const handleSortColumnChange = (event) => {
  setSortColumn(event.target.value);
};

const handleSortOrderChange = (event) => {
  setSortOrder(event.target.value);
};
```

### Render

- The FormControl, InputLabel, and Select components from Material-UI are used for creating dropdowns to select the sort column and order.
- The TableContainer, Table, TableHead, and TableBody components are used to structure the table for displaying sorted user data.
- Conditional rendering is implemented to display a loading message while the data is being fetched.

#### Example:

```jsx
return (
  <>
    {/* Sort dropdowns */}
    <Box sx={{ display: "flex", gap: 2, justifyContent: "end", mt: 2 }}>
      {/* Sort Column dropdown */}
      <FormControl>
        <InputLabel htmlFor="sort-column">Sort Column</InputLabel>
        <Select
          value={sortColumn}
          onChange={handleSortColumnChange}
          inputProps={{
            name: "sort-column",
            id: "sort-column",
          }}
        >
          {/* Options for different columns */}
        </Select>
      </FormControl>

      {/* Sort Order dropdown */}
      <FormControl>
        <InputLabel htmlFor="sort-order">Sort Order</InputLabel>
        <Select
          value={sortOrder}
          onChange={handleSortOrderChange}
          inputProps={{
            name: "sort-order",
            id: "sort-order",
          }}
        >
          {/* Options for ascending and descending order */}
        </Select>
      </FormControl>
    </Box>

    {/* User data table */}
    <TableContainer component={Paper} style={{ marginTop: 20 }}>
      <Table>{/* Table structure */}</Table>
    </TableContainer>
  </>
);
```

#### Example Usage:

```jsx
import UserSort from "src/components/freshy/loginApp/userSort";

const ExampleComponent = () => {
  return <UserSort />;
};
```

This component can be imported and used in other parts of the application where user data needs to be displayed with the ability to sort based on different columns and orders.

## UserSearchPaginationSort Component Documentation

### Overview

The `UserSearchPaginationSort` component combines features for searching, paginating, and sorting user data. It allows users to input a search query, select the number of rows per page, and sort the displayed data based on different columns and orders. The component fetches data from an API endpoint and updates the UI accordingly.

### Component Structure

The component is structured as follows:

- **Dependencies**

  - `@mui/material`: Imports necessary Material-UI components for styling and layout.
  - `react`: Imports the React library.

- **State Variables**

  - `data`: Manages the user data fetched from the API based on search, pagination, and sort settings.
  - `loading`: Indicates whether the data is still being fetched.
  - `search`: Manages the search query entered by the user.
  - `page`: Manages the current page number for pagination.
  - `perPage`: Manages the number of rows to display per page.
  - `sortColumn`: Manages the currently selected column for sorting.
  - `sortOrder`: Manages the selected order for sorting (ascending or descending).
  - `totalPages`: Keeps track of the total number of pages based on the fetched data.

- **Functions**
  - `fetchData`: Performs an asynchronous API call to fetch user data based on the current search, pagination, and sort settings.
  - `handleSearchChange`: Handles the change in the search input.
  - `handlePageChange`: Handles the change in the current page number for pagination.
  - `handleRowsPerPageChange`: Handles the change in the number of rows per page.
  - `handleSortColumnChange`: Handles the change in the selected sort column from the dropdown.
  - `handleSortOrderChange`: Handles the change in the selected sort order from the dropdown.

#### Example:

```jsx
const fetchData = async () => {
  setLoading(true);

  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_APP_HOST}/users?search=${search}&page=${page}&per_page=${perPage}&sort_column=${sortColumn}&sort_order=${sortOrder}`
    );
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const result = await response.json();

    console.info("[INFO] Fetched user, search, pagination, sort successfully");
    console.log("UserSearchPaginationSort", result);
    setData(result.data);
    setTotalPages(result.total_pages);
  } catch (error) {
    console.error("Error fetching data:", error);
  } finally {
    setLoading(false);
  }
};

const handleSearchChange = (event) => {
  setSearch(event.target.value);
};

const handlePageChange = (event, newPage) => {
  setPage(newPage);
};

const handleRowsPerPageChange = (event) => {
  setPerPage(parseInt(event.target.value, 10));
  setPage(1); // Reset page to 1 when changing rows per page
};

const handleSortColumnChange = (event) => {
  setSortColumn(event.target.value);
};

const handleSortOrderChange = (event) => {
  setSortOrder(event.target.value);
};
```

### Render

- The FormControl, InputLabel, and Select components from Material-UI are used for creating dropdowns to select the sort column and order.
- The TextField component is used for the search input.
- The Button component triggers the search when clicked.
- The TableContainer, Table, TableHead, and TableBody components are used to structure the table for displaying user data.
- The Pagination component provides navigation between pages, and the Select component allows users to choose the number of rows per page.

#### Example:

```jsx
return (
  <>
    {/* Search, Sort, and Pagination controls */}
    <Box sx={{ display: "flex", gap: 2, justifyContent: "end", mt: 2 }}>
      {/* Sort Column dropdown */}
      {/* Sort Order dropdown */}
      {/* Search input */}
      {/* Search button */}
    </Box>

    {/* User data table */}
    <TableContainer component={Paper} style={{ marginTop: 20 }}>
      <Table>{/* Table structure */}</Table>
    </TableContainer>

    {/* Rows per page and Pagination */}
    <Box
      sx={{ display: "flex", justifyContent: "space-between", alignItems: "center", mt: 2, gap: 2 }}
    >
      {/* Rows per page dropdown */}
      {/* Pagination component */}
    </Box>
  </>
);
```

#### Example Usage:

```jsx
import UserSearchPaginationSort from "src/components/freshy/loginApp/UserSearchPaginationSort";

const ExampleComponent = () => {
  return <UserSearchPaginationSort />;
};
```

This component can be imported and used in other parts of the application where user data needs to be displayed with search, pagination, and sorting functionality.

## UserDetail Component Documentation

### Overview

The `UserDetail` component allows users to input a user ID, fetch user details from an API endpoint, and display the user's information, including their avatar, name, username, and email. The component provides feedback with loading spinners, error messages, and a visual indicator for highlighting a specific user.

### Component Structure

The component is structured as follows:

- **Dependencies**

  - `@mui/icons-material`: Imports the `Circle` icon from Material-UI for a visual indicator.
  - `@mui/material`: Imports necessary Material-UI components for styling and layout.
  - `react`: Imports the React library.

- **State Variables**

  - `userId`: Manages the user ID input by the user.
  - `userData`: Holds the detailed information of the fetched user.
  - `loading`: Indicates whether data is still being fetched.
  - `error`: Stores error messages in case of unsuccessful API calls.

- **Functions**
  - `fetchUserData`: Performs an asynchronous API call to fetch user details based on the provided user ID.
  - `handleUserIdChange`: Handles changes in the user ID input.
  - `handleKeyPress`: Listens for the "Enter" key press to trigger the `fetchUserData` function.

#### Example:

```jsx
const fetchUserData = async () => {
  setLoading(true);

  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/users/${userId}`);
    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(`HTTP error! Status: ${response.status}, Message: ${errorData.message}`);
    }
    const result = await response.json();

    console.info("[INFO] Fetched user detail successfully");
    console.log("UserDetail", result);
    setUserData(result.user);
  } catch (error) {
    setError(error.message);
  } finally {
    setLoading(false);
  }
};

const handleUserIdChange = (event) => {
  setUserId(event.target.value);
};

const handleKeyPress = (e) => {
  if (e.key === "Enter") {
    fetchUserData();
  }
};
```

### Render

- The `TextField` component is used for user ID input.
- The `Button` component triggers the fetchUserData function.
- The `CircularProgress` component shows a loading spinner during data fetching.
- The `Typography` component displays error messages.
- The user data is displayed using a combination of `Box` and `Typography` components, including the user's avatar, ID, name, username, and email. The Circle icon is used as a visual indicator.

#### Example:

```jsx
return (
  <>
    {/* User ID input */}
    <TextField
      label="User ID"
      variant="outlined"
      type="number"
      value={userId}
      onChange={handleUserIdChange}
      onKeyDown={handleKeyPress}
      sx={{ my: 2 }}
    />
    {/* Fetch User button */}
    <Button variant="contained" color="primary" onClick={fetchUserData}>
      Fetch User
    </Button>
    {/* Loading spinner */}
    {loading && <CircularProgress style={{ marginTop: 20 }} />}
    {/* Error message */}
    {error && (
      <Typography variant="body1" color="error">
        {error}
      </Typography>
    )}
    {/* User data */}
    {userData && (
      <Box sx={{ display: "flex", gap: 4, mt: 4 }}>
        {/* Avatar with Circle icon */}
        <Box sx={{ position: "relative" }}>
          <img
            src={userData?.avatar}
            alt="Avatar"
            style={{
              width: "120px",
              height: "120px",
              borderRadius: "50%",
              outline: "1px solid #62626240",
              outlineOffset: "5px",
            }}
          />
          <Circle
            sx={{
              position: "absolute",
              right: 0,
              bottom: 6,
              color: "#23c369",
              border: "1px solid #62626240",
              outline: "3px solid white",
              borderRadius: "50%",
              outlineOffset: -4,
              fontSize: "1.75em",
            }}
          />
        </Box>
        {/* User details */}
        <Box>
          <Typography variant="button">{`ID: ${userData?.id}`}</Typography>
          <Typography variant="h6" mb={1}>
            Name: {userData?.fname} {userData?.lname}
          </Typography>
          <Typography variant="body1">Username: {userData?.username}</Typography>
          <Typography variant="body1">Email: {userData?.email}</Typography>
        </Box>
      </Box>
    )}
  </>
);
```

#### Example Usage:

```jsx
import UserDetail from "src/components/freshy/loginApp/UserDetail";

const ExampleComponent = () => {
  return <UserDetail />;
};
```

This component can be imported and used in other parts of the application where detailed user information needs to be fetched and displayed.

## SignupComponent Documentation

### Overview

The `SignupComponent` is a form component designed for user registration. It includes form fields for first name, last name, email, and password. Users can also upload an avatar, and there's a preview of the selected avatar with the ability to change it. The form validation is done using the `Yup` library. After successful registration, the user is shown a congratulatory message.

### Component Structure

The component is structured as follows:

- **Dependencies**

  - `react`: Imports the React library.
  - `useState`: Hook for managing component state.
  - `useFormik`: A small library that helps you with the management of form state and validation using React.
  - `Yup`: JavaScript schema builder for value parsing and validation.
  - `@mui/material`: Imports necessary Material-UI components for styling and layout.
  - `@mui/icons-material`: Imports icons from Material-UI.

- **State Variables**

  - `isRegister`: Manages the display of the registration success message.
  - `loading`: Indicates whether the form submission is in progress.
  - `avatar`: Holds the avatar file selected by the user.
  - `avatarPreview`: Holds the preview URL of the selected avatar.
  - `fileSize`: Holds the size of the selected avatar file.

- **Functions**
  - `handleAvatarChange`: Handles changes in the selected avatar file.
  - `sendUserData`: Sends user data to the server for registration.
  - `handleReset`: Resets the form after successful registration.

#### Example:

```jsx
const handleAvatarChange = (event) => {
  // Implementation...
};

const sendUserData = async (values, avatarBase64) => {
  try {
    setLoading(true);
    const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/users/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        fname: values.fname,
        lname: values.lname,
        username: values.email,
        password: values.password,
        email: values.email,
        avatar: avatarBase64,
      }),
    });

    const data = await response.json();

    if (response.ok) {
      console.info("[INFO] User created successfully");
      setSnackbarMessage({ status: "success", title: "Register successful" });
      handleReset();
    } else {
      console.error("[ERROR] User creation failed:", data.message);
      setSnackbarMessage({ status: "failed", title: `User creation failed: ${data.message}` });
      formik.setErrors({ submit: data.message });
    }
    setLoading(false);
    setOpenSnackbar(true);
  } catch (err) {
    console.error("[ERROR] Error during user creation:", err);
    formik.setErrors({ submit: "An error occurred during user creation." });
  }
};

const handleReset = () => {
  // Implementation...
};
```

### Formik Configuration

- `useFormik` is used for managing form state, form validation, and handling form submission.

#### Example:

```jsx
const formik = useFormik({
  initialValues: {
    email: "",
    fname: "",
    lname: "",
    password: "",
    submit: null,
  },
  validationSchema: Yup.object({
    // Validation schema for form fields
  }),
  onSubmit: async (values, helpers) => {
    // Form submission logic
  },
});
```

### Render

- The component renders different content based on whether the registration is successful (`isRegister` state).
- If registration is successful, it displays a congratulatory message and a button to go back to the registration form.
- If registration is not successful, it renders a form with input fields for first name, last name, email, password, and an avatar upload feature.
- Avatar preview and size information are displayed dynamically.
- A submit button is provided, and it shows a loading spinner during form submission.

#### Example:

```jsx
return (
    <>
        {/* Conditional rendering based on registration status */}
        {isRegister ? (
            {/* Congratulatory message */}
            <>
                <Typography variant="h5" mt={4}>Congrats!</Typography>
                <Typography color="text.secondary" variant="body2">
                    You created an account successfully.
                </Typography>
                <Button
                    fullWidth
                    size="large"
                    sx={{ mt: 3 }}
                    variant="contained"
                    onClick={() => setIsRegister(false)}
                >
                    Back to create again!
                </Button>
            </>
        ) : (
            {/* Registration form */}
            <form noValidate onSubmit={formik.handleSubmit}>
                {/* Avatar preview */}
                {/* Form fields for first name, last name, email, and password */}
                {/* Error messages for form fields */}
                {/* Submit button with loading spinner */}
            </form>
        )}
    </>
);
```

#### Example Usage:

```jsx
import SignupComponent from "src/components/freshy/loginApp/SignupComponent";

const ExampleComponent = () => {
  return (
    <SignupComponent setSnackbarMessage={setSnackbarMessage} setOpenSnackbar={setOpenSnackbar} />
  );
};
```

This component can be imported and used in other parts of the application where user registration is required.

## UserUpdate Component Documentation

### Overview

The `UserUpdate` component is designed to update user information, including the ability to update the user's avatar. It provides a form with fields to choose a user from a list, update their avatar, first name, last name, email, and password. After updating, it displays the updated user information along with the new avatar. It also handles errors and loading states during the update process.

### Component Structure

The component is structured as follows:

- **Dependencies**

  - `React`: Imports the React library.
  - `useState`, `useEffect`: Hooks for managing component state and handling side effects.
  - `@mui/material`: Imports Material-UI components for styling and layout.
  - `@mui/icons-material`: Imports icons from Material-UI.
  - `defaultAvatar`: Imports a default avatar image.

- **State Variables**

  - `updating`: Indicates whether the update process is in progress.
  - `response`: Holds the response received after attempting the user update.
  - `userData`: Holds the data of all users fetched from the API.
  - `selectedUser`: Holds the data of the currently selected user for update.
  - `avatar`: Holds the updated avatar file.
  - `avatarPreview`: Holds the preview URL of the selected avatar.
  - `fileSize`: Holds the size of the selected avatar file.
  - `updated`: Indicates whether the update was successful.

- **Functions**
  - `handleAvatarChange`: Handles changes in the selected avatar file.
  - `handleUpdateUser`: Handles the process of updating the user.

#### Example:

```jsx
const handleAvatarChange = (event) => {
  // Implementation...
};

const handleUpdateUser = async () => {
  // Implementation...
};
```

### UseEffect Hook:

- Fetches user data from the API when the component mounts.

#### Example:

```jsx
useEffect(() => {
  const fetchUserData = async () => {
    // Implementation...
  };

  fetchUserData();
}, [userData]);
```

### Render

- Renders a form with input fields for updating user information.
- Displays a list of users in a dropdown menu.
- Allows the user to update the avatar, first name, last name, email, and password.
- Displays avatar preview and size information dynamically.
- Shows loading spinner during the update process.
- Displays the updated user information and avatar after a successful update.

#### Example:

```jsx
return (
    <>
        {/* Conditional rendering based on update status */}
        {!updated ? (
            {/* Update form */}
            <form>
                {/* Form fields and controls */}
            </form>
        ) : (
            {/* Updated user information */}
            <>
                {/* Loading spinner */}
                {/* Updated user information and avatar */}
                {/* Back to Update button */}
            </>
        )}
    </>
);
```

#### Example Usage:

```jsx
import UserUpdate from "src/components/freshy/loginApp/UserUpdate";

const ExampleComponent = () => {
  return <UserUpdate />;
};
```

This component can be imported and used in other parts of the application where user information needs to be updated.

## UserDelete Component Documentation

### Overview

The `UserDelete` component is designed to delete a user from a list of users fetched from an API. It provides a dropdown menu to choose a user for deletion and displays user information, including an avatar, before confirming the deletion. The component also includes a delete button, and a Snackbar to display success or error messages after deletion.

### Component Structure

The component is structured as follows:

- **Dependencies**

  - `React`: Imports the React library.
  - `useState`, `useEffect`: Hooks for managing component state and handling side effects.
  - `@mui/material`: Imports Material-UI components for styling and layout.
  - `@mui/icons-material`: Imports icons from Material-UI.

- **State Variables**

  - `deleting`: Indicates whether the delete process is in progress.
  - `snackbarOpen`: Controls the visibility of the Snackbar.
  - `snackbarMessage`: Holds the message to be displayed in the Snackbar.
  - `userList`: Holds the data of all users fetched from the API.
  - `selectedUserId`: Holds the ID of the currently selected user for deletion.
  - `selectedUser`: Holds the data of the currently selected user for deletion.

- **Functions**
  - `handleDelete`: Handles the process of deleting the selected user.
  - `handleUserSelect`: Handles the selection of a user from the dropdown menu.

#### Example:

```jsx
const handleDelete = async () => {
  setDeleting(true);

  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/users/delete`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: selectedUserId,
      }),
    });

    const result = await response.json();

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}, Message: ${result.message}`);
    }

    setSnackbarMessage(result.message);
    setSnackbarOpen(true);
  } catch (error) {
    setSnackbarMessage(error.message);
    setSnackbarOpen(true);
  } finally {
    setDeleting(false);
    // Reset state
    setSelectedUserId("");
    setSelectedUser(null);
    setUserList([]);
  }
};

const handleUserSelect = (event) => {
  const selectedUser = userList.find((user) => user.id === event.target.value);
  setSelectedUserId(event.target.value);
  setSelectedUser(selectedUser);
};
```

### UseEffect Hook

- Fetches user data from the API when the component mounts.

#### Example:

```jsx
useEffect(() => {
  const fetchUserList = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/users`);
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      const result = await response.json();
      setUserList(result);
    } catch (error) {
      console.error("Error fetching user list:", error);
    }
  };

  fetchUserList();
}, [userList]);
```

### Render

- Renders a dropdown menu to select a user for deletion.
- Displays user information, including an avatar, before confirming deletion.
- Shows loading spinner during the deletion process.
- Displays the delete button and a Snackbar for success or error messages.

#### Example:

```jsx
return (
  <>
    {/* Dropdown menu for user selection */}
    {/* User information display */}
    {/* Delete button */}
    {/* Snackbar for success or error messages */}
  </>
);
```

#### Example Usage:

```jsx
import UserDelete from "src/components/freshy/loginApp/UserDelete";

const ExampleComponent = () => {
  return <UserDelete />;
};
```

This component can be imported and used in other parts of the application where user deletion functionality is needed.