import Head from 'next/head';
import Login from 'src/components/pang/Login';

export default function Home() {
  const handleLogin = (jwt) => {
    console.log('JWT:', jwt);
  };

  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100vh',
    }}>
      <Head>
        <title>Login Workshop</title>
        <meta name="description" content="Login Workshop" />
      </Head>

      <main>
        <Login onLogin={handleLogin} />
      </main>
    </div>
  );
}
