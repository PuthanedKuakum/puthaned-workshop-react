
# Orn's Workshop

## Overview

การฝึกใช้ React Next.js MUI ผ่านการทำ workshop ต่างๆ

## Table of Contents

- [Getting Started](#getting-started)
  - [Collaborate with team](#Collaborate-with-team)
  - [Installation](#installation)
- [Folder Structure](#folder-structure)
- [Development](#development)
  - [resume](#resume)
  - [workshop 1 เครื่องคิดเลข](#workshop-1-เครื่องคิดเลข)
  - [workshop 2](#workshop-2)
  - [workshop 3](#workshop-3)
  - [workshop 4](#workshop-4)
  - [workshop 5](#workshop-5)

## Getting Started

### Collaborate with team

- หลังจากลง git ตั้งค่า git ผ่านทาง terminal
```
git config --global user.name "username"
git config --global user.email "email"
```

- ทำการสร้าง fork ของตนเองจาก Main และ clone จาก fork ของตนเอง
```
git clone https://gitlab.com/H0RosH1/workshop-react.git
```
- pull upstream main ที่ถูก merge แล้วจาก global เข้าสู่ local

```
git remote add upstream https://gitlab.com/H0RosH1/workshop-react.git
git remote --v 
git pull upstream main
```
- สร้าง branch ที่จะเพิ่มงานของตนเอง
```
git branch orn
git checkout orn
```

### Installation
```
npm installl   
npm install @mui/material @emotion/react @emotion/styled
```
## Folder Structure
    workshop-react/
    ├─ node_modules/
    ├─ public/
    │  ├─ orn/
    │  │  ├─ logo-14 1.png
    │  │  ├─ profile.jpg
    │  ├─ index.html
    ├─ src/
    │  ├─ component/
    │  │  ├─ orn/
    │  │     ├─ Fetch.js
    │  │     ├─ index.js
    │  │     ├─ navbar.js
    │  │     ├─ navbarWork2.js
    │  │     ├─ work.js
    │  │     ├─ workshop.js
    │  ├─ layouts/
    │  │  ├─ dashboad/
    │  │     ├─ layout.js
    │  ├─ pages/
    │  │  ├─ workShop/
    │  │     ├─ onrain/
    │  │        ├─ index.js   
    │  ├─ styles/
    │  │  ├─ orn/
    │  │     ├─ work.module.css
    │  │  ├─ globals.css
    │  ├─ index.js
    ├─ .gitignore
    ├─ .env
    ├─ package.json
    ├─ README.md

## Development

### resume
#### ไฟล์ src/page/workShop/onrain/index.js

- import layout และ components orn

```
import { Layout } from "src/layouts/dashboard/layout";
import Orn from "src/components/orn";
```
- return component Orn และ ใช้ Layout
  
```
const onrain = () => {
    return(
       <><Orn/></>
    )
}
onrain.getLayout = (onrain) => <Layout>{onrain}</Layout>;
export default onrain;
```

#### ไฟล์ src/component/orn/index.js

- import Navbar และ styles
```
import Navbar from './navbar';
import styles from 'src/styles/profile.module.css';
```

- สร้าง function และ Array เก็บข้อมูลสำหรับใส่ใน Table
```
const createData = (number, title, credits) => {
    return { number, title, credits };
}

const rows = [
    createData('342117', 'STRUCTURED PROGRAMMING FOR INFORMATION TECHNOLOGY', 3),
    createData('342181', 'LOGIC AND CONCEPTS', 3),
    createData('342182', 'INSPIRATION IN IT CAREER', 1),
    createData('GE141166', 'SCIENCE OF HAPPINESS', 3),
    createData('GE151144', 'MULTICULTURALISM', 3),
    createData('GE321415', 'LEARNING SKILLS', 3),
    createData('LI101001', 'ENGLISH I ', 3),
];
```
- สร้างตัวแปรสำหรับ pagination ของข้อมูลใน Table ภายใน function Orn()

```
const [currentPage, setCurrentPage] = useState(1);
const dataPerPage = 5; // กำหนดจำนวนข้อมูลที่โชว์ในแต่ละหน้า
const start = (currentPage - 1) * dataPerPage;
const end = start + dataPerPage;
const paginatedData = rows.slice(start, end);
const handlePagination = (event,page) => {
    setCurrentPage(page);
};
```
- แสดงข้อมูลที่กำหนด pagination แล้วใน table โดย return TableBody ภายใต้แท็ก < Table >  </ Table > ภายใน function Orn()

```
<TableBody>
    {paginatedData.map((row) => (
        <TableRow
            key={row.number}
            sx={{ '&:last-child td, &:last-child th': { border: 0}}}>
            <TableCell>{row.number}</TableCell>
            <TableCell component="th" scope="row">
                {row.title}
            </TableCell>
            <TableCell align='center'>{row.credits}</TableCell>
        </TableRow>
    ))}
</TableBody>
```
- return pagination
```
<Pagination
        count={Math.ceil(rows.length / dataPerPage)}
        page={currentPage}
        onChange={handlePagination}
/>
```
#### ไฟล์ src/component/orn/navbar.js

- return Link ไปสู่ workshop ต่างๆ ภายใน function Navbar()
```
<Link href="/workShop/onrain/workshop2" 
    className={styles.link}
    sx={{ my: 2, display: 'block' }}>
        workshop2
</Link>
```
### workshop 1 เครื่องคิดเลข
#### ไฟล์ src/page/workShop/onrain/workpage.js

- return component Work จากไฟล์ src/component/orn/work.js ภายใน function WorkPage()

#### ไฟล์ src/component/orn/work.js

- สร้างตัวแปรเก็บ Array ตัวเลขที่ใช้ในเครื่องคิดเลข
```
const number = [1, 2, 3, 4, 5, 6, 7, 8, 9,'00', 0,'.'];
```
- ภายใน function Work() สร้างตัวแปรเก็บค่าที่ใช้ในการคำนวณและผลลัพธ์ และแสดงข้อมูลผ่าน Element ที่มี id คือ result

```
// เก็บค่าที่ใช้คำนวณ
let storedString = '';
// เก็บผลลัพธ์
let result = '';

// ต่อ string ค่าที่กรอก
const store = (item) => {
    storedString += item;
    console.log(storedString);
    console.log(result);
    // แสดงข้อมูลทาง UI
    document.getElementById("result").innerHTML = storedString;
}
```
- คำนวณตัวเลขที่ถูกกรอกเข้ามา
```
const calculate = (storedString) => {
        try {
            result = eval(storedString);
        } catch (error) {
            result = "ไม่มีการกำหนดผลลัพธ์"
        }
        console.log(storedString);
        console.log(result);
        document.getElementById("result").innerHTML = result;
    }
```
- ปุ่ม reset
```
const reset = () =>{
        result = '';
        storedString = '';
        console.log(storedString);
        console.log(result);
        document.getElementById("result").innerHTML = storedString;
    }
```
- ปุ่ม reverse (+/-) 
```
const reverse = () =>{
        // เรียกตัวเลขที่แสดงผลอยู่
        let checknum = document.getElementById("result").innerHTML
        console.log(checknum)
        let newNum = 0
        // เช็คว่ามีเครื่องหมายลบอยู่ข้างหน้าหรือไม่ ถ้าไม่มีให้ใส่เครื่องหมายลบข้างหน้า
        if(checknum.substring(0,1) != '-'){
            if(checknum == storedString){
                storedString = "-"+storedString
                document.getElementById("result").innerHTML = storedString
            }else if(checknum == result){
                newNum = -result
            }
        // ถ้ามีเครื่องหมายลบข้างหน้าอยู่แล้วให้ลบออก
        }else if(checknum.substring(0,1) == '-'){
            if(checknum == storedString){
                storedString = storedString.slice(2)
                document.getElementById("result").innerHTML = storedString
            }else if(checknum == result){
                newNum = result.slice(2)
            }
        }
       
        
    }
```
- ปุ่มหา %

```
const percent = () =>{
        result = storedString / 100
        console.log(storedString);
        console.log(result);
        document.getElementById("result").innerHTML = result;
    }
```
- return ตัวเลขที่ต้องการมีในเครื่องคิดเลข
```
{number.map((item) => (
        <button key={item}
            onClick={() => store(item)}>{item}
        </button>
))}
```

### workshop 2
ทำหน้า UI ตามตัวอย่างที่ถูกออกแบบไว้ใน 
```
https://www.figma.com/file/JbV1tKPt8CZXQdgp5EghEd/Home-Interior-Design-Website-Wireframe-(Community)?type=design&node-id=0-1&mode=design&t=6SFVcbSrR6SI2WCY-0  
```
2 หน้า 
#### - หน้าแรก -
#### ไฟล์ src/page/workShop/onrain/workshop2.js

- return component Workshop2Compo จากไฟล์ src/component/orn/workshop2.js
```
import Workshop2Compo from 'src/components/orn/workshop2';
```

#### ไฟล์  src/component/orn/workshop2.js

- import component NavbarWork2 และ FooterWork2 

```
import NavbarWork2 from "./navbarWork2";
import FooterWork2 from "./footerWork2";
```
#### - หน้า 2 -
#### ไฟล์ src/page/workShop/onrain/workshop2page2.js

- return component Work2page2 จากไฟล์ src/components/orn/work2page2 
```
import Work2page2 from "src/components/orn/work2page2";
```

#### ไฟล์  src/component/orn/workshop2.js
- import component NavbarWork2 และ FooterWork2

### workshop 3
ทำระบบ login โดยใช้ API
```
https://www.melivecode.com/api/login
```

โดยข้อมูลที่ต้องใส่คือ
```
{
  "username": "karn.yong@melivecode.com",
  "password": "melivecode"
}
```
แสดง log การทำงานของระบบ login และสร้างไฟล์ .env
โดย log จะมี info request error
และให้เก็บ jwt ลง localstorage

#### ไฟล์ src/page/workShop/onrain/workshop3.js

- return component Workshop3Compo จากไฟล์ src/components/orn/workshop3.js
```
import Workshop3Compo from "src/components/orn/workshop3"
```

#### ไฟล์  src/component/orn/workshop3.js

- import component Crud จากไฟล์ src/component/orn/Fetch.js และ swal จาก sweetalert

```
import { Crud } from './Fetch';
import swal from 'sweetalert';
```
- install sweetalert
```
npm i sweetalert
```
- ภายใน function Workshop3Compo() สร้าง useState เก็บข้อมูล username และ password
```
const [username, setUserName] = useState();
const [password, setPassword] = useState();
```
- สร้าง function สำหรับการเข้าสู่ระบบ โดยจะต้องอยู่ภายใน function Workshop3Compo() เช่นกัน
```
const handleSubmit = async e => {
    e.preventDefault();

    // เรียกใช้ Crud() 
    const response = await Crud(`${process.env.NEXT_PUBLIC_AUTH_URL}`,{
      username,
      password
    },'POST');
   
    try {
    //  ถ้ามี accessToken ใน response ให้ setItem ลงใน localStorage
      if ('accessToken' in response) {
        swal("Success", response.message, "success", {
          buttons: false,
          timer: 2000,
        })
          .then((value) => {
            localStorage.setItem('accessToken', response['accessToken']);
            console.log('info :', response.message, response);
            window.location.href = "/workShop/onrain/workshop4";
          });
      }
      else {
        // validate ช่องกรอก
        if (username.trim() === '' || !validator.isEmail(username)) {
          swal("Failed", "email ไม่ถูกต้อง", "error")
        }
        else if (password.trim() === '') {
          swal("Failed", "password ไม่ถูกต้อง", "error")
        }
        else {
          swal("Failed", response.message + '\n' + 'บัญชีผู้ใช้ไม่ถูกต้อง', "error");
        }
        console.log("error : "+ response.message)
      }
    } catch (error) {
      console.log(error)
    }
   
  }
```
- เพิ่ม link api ใน .env
```
NEXT_PUBLIC_AUTH_URL = https://www.melivecode.com/api/login
```
- return form 

```
<form onSubmit={handleSubmit} noValidate>
    <TextField onChange={e => setUserName(e.target.value)} />
    <TextField onChange={e => setPassword(e.target.value)} />
    <Button type="submit"> Sign In </Button>
</form>
```
#### ไฟล์ src/component/orn/Fetch.js
รวม function สำหรับ return fetch data 
- สำหรับเรียกใช้ข้อมูล method GET
```
export const GetData = async (api) => {
    return fetch(api, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(data => data.json())

}
```
- สำหรับการ create, update, and delete
```
export const Crud = async (api, body, method) => {
    return fetch(api, {
        method: method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
    })
        .then(data => data.json())
}
```
- สำหรับการเข้าสู่ระบบ (ใช้ accessToken)
```
export async function User(api) {
    const accessToken = localStorage.getItem('accessToken');

    return fetch(api, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
        }
    })
        .then(data => data.json())
}
```
### workshop 4
ใช้ jwt ที่ได้จาก login ที่ทำมาดึงข้อมูลจาก API
``` 
https://www.melivecode.com/api/auth/user
```
นำข้อมูลมาแสดงและเก็บลง localstorage และ log ออกมา
#### ไฟล์ src/page/workShop/onrain/workshop4.js
- return component Workshop4Compo จากไฟล์ src/components/orn/workshop4.js

#### ไฟล์ src/component/orn/workshop4.js

- import function User() จากไฟล์ src/component/orn/Fetch.js
```
import User from './Fetch'
```
- ภายใน function Workshop4Compo() สร้าง useState เก็บข้อมูล user
```
const [userData, setUserData] = React.useState(null);
```
- ใช้ useEffect เพื่อ render ข้อมูลที่ต้องการจาก api และแสดงผ่าน console
```
React.useEffect(() => {
    // เรียกใช้ User()
        User(`${process.env.NEXT_PUBLIC_AUTH_USER}`).then(data => {
            // เพิ่มข้อมูลสู่ localStorage
            localStorage.setItem('user', JSON.stringify(data.user));
            // แปลงข้อมูลที่ได้มาเพื่อแสดงผลผ่าน console และเพิ่มเข้า userData
            const userstored = localStorage.getItem('user')
            const userinfo = JSON.parse(userstored)
            console.log("info: " + userstored)
            setUserData(userinfo);
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });
    }, []);
```
- เพิ่ม link api ใน .env
```
NEXT_PUBLIC_AUTH_USER = https://www.melivecode.com/api/auth/user
```
- การ logout

```
// เก็บข้อมูลที่ถูกดึงมาจาก localStorage ที่ถูกเพิ่มข้อมูลไว้
const user = localStorage.getItem('user');
const accessToken = localStorage.getItem('accessToken');
const handleLogout = () => {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("user");
    // เปลี่ยนไปที่หน้า login
    window.location.href = "/workShop/onrain/workshop3";
};
```
- สร้างเงื่อนไขหากยังไม่ login จะไม่แสดงหน้าข้อมูล user แต่จะเรียก component ที่ใช้ login แทน
```
if (user === 'undefined' || !user || !accessToken) {
    return <Workshop3/>
}else{
    return(
        // การออกแบบที่ต้องการแสดงข้อมูล user
    )
}
```
- return ข้อมูล user ภายใน function Workshop4Compo() 
```
{userData && (
    <Box>
        <Avatar src={userData.avatar} />
        <Box>
            <Typography variant="h6" component="div">
                name: {userData.fname} {userData.lname}
            </Typography>
            <Typography component="p">
                username: {userData.username}
            </Typography>          
        </Box>
        <Button onClick={handleLogout}>Logout</Button>
    </Box>
)}
```
### workshop 5
ทำการใช้ api ทุกเส้นให้เป็นแบบปุ่มกดใช้งาน แต่ละ api ( ยกเว้น jwt ) โชว์ log เรียก funtion ที่มีการเรียกเส้น api แต่ละตัว ผ่านการกดปุ่ม โดยเรียก api จาก
```
https://www.melivecode.com/
```
#### ไฟล์ src/page/workShop/onrain/workshop5.js
- return component Workshop5Compo จากไฟล์ src/components/orn/workshop5.js

#### ไฟล์ src/component/orn/workshop5.js

- import function Crud และ GetData จากไฟล์ src/component/orn/Fetch.js 
- import swal จาก sweetalert
```
import swal from "sweetalert";
import { GetData, Crud } from "./Fetch";
```
- สร้างตัวแปรเก็บข้อมูลสำหรับใช้ method POST, PUT, and delete ในหัวข้อ User
```
const createUser = {
    "fname": "Cat",
    "lname": "Chat",
    "username": "cat.chat@melivecode.com",
    "password": "12345",
    "email": "cat.chat@melivecode.com",
    "avatar": "https://www.melivecode.com/users/cat.png"
}

const updateUser = {
    "id": 15,
    "lname": "Gato"
}

const deleteUser = {
    "id": 15
}
```
- ภายใน function Workshop5Compo() สร้าง useState เพื่อเก็บข้อมูล
```
const [userListData, setUserListData] = React.useState(null);
const [userPage, setUserPage] = React.useState(null);
const [userDetail, setUserDetail] = React.useState(null);
const [userData, setUserData] = React.useState(null);

const [userCreate, setUserCreate] = React.useState({});
const [userUpdate, setUserUpdate] = React.useState({});
```
- สร้าง function fetchData() เพื่อเรียกข้อมูลมาเพิ่มใน userListData โดยตั้งค่าให้ userDetail, userPage, และ userData เป็นค่า null
```
 const fetchData = (api) => {
        GetData(api).then(data => {
            setUserPage(null)
            setUserListData(data);
            setUserDetail(null)
            setUserData(null);
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });
    }
```
- สร้าง function userList() เพื่อเรียกข้อมูลมาเพิ่มใน userData โดยตั้งค่าให้ userPage และ userDetail เป็น null
```
 const userList = (api) => {
        GetData(api).then(data => {
            setUserPage(null)
            setUserData(data);
            setUserDetail(null)
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });
    }
```
- สร้าง function crudData() เพื่อใช้เรียก function Crud จาก Fetch 
```
const crudData = (api, body, method) => {
        Crud(api, body, method).then(data => {
            setUserPage(null)
            setUserListData(data)
            setUserDetail(null)
            setUserData(null);
        }).catch(error => {
            console.error('Error fetching user data:', error);
        });
    }
```
- สร้าง function dataPage() เพื่อเพิ่มค่าเข้าไปใน userPage เพื่อเรียก pagination 
```
const dataPage = (api) => {
        GetData(api).then(data => {
            setUserListData(data);
            setUserPage(data)
            setUserDetail(null)
            setUserData(null);
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });

    }
```
- สร้าง function dataSearchPageSort() เพื่อเพิ่มค่าเข้าไปใน userPage และ userListData

```
const dataSearchPageSort = () => {
        GetData(`${process.env.NEXT_PUBLIC_SEARCH_PAGE_SORT}`).then(data => {
            setUserListData(data);
            setUserPage(data)
            setUserDetail(null)
            setUserData(null);
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });

    }
```
- เพิ่ม link api ใน .env
```
NEXT_PUBLIC_SEARCH_PAGE_SORT = https://www.melivecode.com/api/users?search=ka&page=1&per_page=10&sort_column=id&sort_order=desc
```
- สร้าง function dataDetail() เพื่อเพิ่มค่าเข้าไปใน userListData และ userDetail
```
 const dataDetail = () => {
        GetData(`${process.env.NEXT_PUBLIC_USER_DETAIL}`).then(data => {
            setUserPage(null)
            setUserListData(data);
            setUserDetail(data)
            setUserData(null);
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });
    }
```
- เพิ่ม link api ใน .env
```
NEXT_PUBLIC_USER_DETAIL = https://www.melivecode.com/api/users/1
```
- สร้าง useEffect เพื่อใช้ log ข้อมูลออกมา
```
 React.useEffect(() => {
        console.log(userListData);

        // log ข้อมูล user
        if (userData != null) {
            console.log(userData);
        }
        // log ข้อมูล user pagination
        if (userPage != null) {
            if (Array.isArray(userPage.data)) {
                const hasUserName = userPage.data.some(user => user.name !== undefined);
                if (hasUserName) {
                    const names = userPage.data.map((user) => user.name);
                    console.log(names);
                } else if (!hasUserName) {
                    const names = userPage.data.map((user) => user.fname + ' ' + user.lname);
                    console.log(names);
                }

            }
        }
        if (userDetail != null) {
            const names = userDetail.user.fname + ' ' + userDetail.user.lname
            console.log(names);
        }

    }, [userListData, userPage, userDetail, userData]);
```
- สร้าง function handleInputChange() เพื่อเพิ่มข้อมูลที่ถูกกรอกเข้ามาใน form ลงใน userCreate
```
 const handleInputChange = (key, value) => {
        setUserCreate(prevState => ({
            ...prevState,
            [key]: value,
            avatar: 'https://www.melivecode.com/users/12.png'
        }));
    };
```
- สร้าง function handleSubmit() เพื่อเรียก function Crud() และเพิ่ม parameter เป็น userCreate สำหรับ Create ข้อมูล
```
const handleSubmit = async e => {
        e.preventDefault();

        try {
            const res = await Crud(`${process.env.NEXT_PUBLIC_USER_CREATE}`, userCreate, 'POST');
            console.log(res);
            if (res.status == 'ok') {
                swal("Success", res.message, "success", {
                    buttons: false,
                    timer: 2000,
                })
            } else {
                swal("Failed", res.message, "error", {
                    buttons: false,
                    timer: 2000,
                })
            }

        } catch (error) {
            console.error("Error:", error);
        }

    }
```
- เพิ่ม link api ใน .env
```
NEXT_PUBLIC_USER_CREATE = https://www.melivecode.com/api/users/create
```
- สร้าง useState สำหรับเก็บข้อมูล เปิด-ปิด modal แก้ไขข้อมูล
```
const [open, setOpen] = React.useState(false);
```
- สร้าง function handleClickOpen() ใช้เปิด modal และเรียกข้อมูลของ User ที่เลือกเพื่อแก้ไขข้อมูล
```
const handleClickOpen = (data) => {
        setOpen(true)
        setUserUpdate(prevState => ({
            ...prevState,
            'id': data.id,
            'fname': data.fname,
            'lname': data.lname,
            'username': data.username,
            avatar: 'https://www.melivecode.com/users/12.png'
        }));
        console.log(userUpdate)
    };
```
- สร้าง function handleUpdateChange() เพื่อเก็บข้อมูลที่ถูกแก้ไขลงใน userUpdate
```
const handleUpdateChange = (key, value, id) => {
        setUserUpdate(prevState => ({
            ...prevState,
            [key]: value,
        }));
    };
```
- สร้าง function handleSubmitUpdate() เพื่อเรียกใช้ function Crud() และเพิ่ม parameter เป็น userupdate เพื่อ Update ข้อมูล และปิด modal
```
const handleSubmitUpdate = async e => {
        e.preventDefault();

        try {
            const res = await Crud(`${process.env.NEXT_PUBLIC_USER_UPDATE}`, userUpdate, 'PUT');
            console.log(res);
            setOpen(false)  // ปิด modal
            if (res.status == 'ok') {
                swal("Success", res.message, "success", {
                    buttons: false,
                    timer: 2000,
                }).then((value) => {
                    window.location.reload();
                })
            } else {
                swal("Failed", res.message, "error", {
                    buttons: false,
                    timer: 2000,
                })
            }
            
        } catch (error) {
            console.error("Error:", error);
        }

    }
```
- เพิ่ม link ใน .env
```
NEXT_PUBLIC_USER_UPDATE = https://www.melivecode.com/api/users/update
```
- ปุ่มปิด modal
```
const handleClose = () => setOpen(false);
```
- การเปิด-ปิด modal สำหรับ delete
```
const [openDelete, setOpenDelete] = React.useState(false);
const [idToDelete, setIdToDelete] = React.useState()

const handleOpenDelete = (id) => {
    setOpenDelete(true);
    setIdToDelete(id)
};

const handleCloseDelete = () => {
    setOpenDelete(false);
};

```
- สร้าง function handleDelete() เพื่อเรียกใช้ function Crud() และเพิ่ม parameter เป็น id ที่ต้องการลบ และปิด modal 
```
const handleDelete = async (id) => {

        try {
            const res = await Crud(`${process.env.NEXT_PUBLIC_USER_DELETE}`, { "id": id }, 'DELETE');
            console.log(res);
            // ปิด modal
            setOpenDelete(false);
            if (res.status == 'ok') {
                swal("Success", res.message, "success", {
                    buttons: false,
                    timer: 2000,
                }).then((value) => {
                    window.location.reload();
                })
            } else {
                swal("Failed", res.message, "error", {
                    buttons: false,
                    timer: 2000,
                })
            }

        } catch (error) {
            console.error("Error:", error);
        }

    }
```
- เพิ่ม link ใน .env
```
NEXT_PUBLIC_USER_DELETE = https://www.melivecode.com/api/users/delete
```
- return ปุ่มที่ใช้เรียก function ต่างๆ
```
<Button onClick={() => userList(`${process.env.NEXT_PUBLIC_USER_LIST}`)}>user list</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_USER_SEARCH}`)}>user search</Button>
<Button onClick={() => dataPage(`${process.env.NEXT_PUBLIC_PER_PAGE}`)}>page</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_SORT}`)}>user sort</Button>
<Button onClick={() => dataSearchPageSort()}>user search+page+sort</Button>
<Button onClick={() => dataDetail()}>user detail</Button>
<Button onClick={() => crudData(`${process.env.NEXT_PUBLIC_USER_CREATE}`, createUser, 'POST')}>user create</Button>
<Button onClick={() => crudData(`${process.env.NEXT_PUBLIC_USER_UPDATE}`, updateUser, 'PUT')}>user update</Button>
<Button onClick={() => crudData(`${process.env.NEXT_PUBLIC_USER_DELETE}`, deleteUser, 'DELETE')}>user delete</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_LIST}`)}>attractions list</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_SEARCH}`)}>attractions search</Button>
<Button onClick={() => dataPage(`${process.env.NEXT_PUBLIC_ATTRACTION_PAGE}`)}>attractions page</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_LANGUAGE}`)}>attractions language</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_DETAIL}`)}>attractions detail</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_LANGUAGE_DETAIL}`)}>attractions language detail</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_STATIC_PATH}`)}>attractions static paths</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_PET_SALES_LIST}`)}>pet sales summary</Button>
<Button onClick={() => fetchData(`${process.env.NEXT_PUBLIC_PET_SALES_DAILY_LIST}`)}>pet sales daily</Button>
```
- เพิ่ม link api ใน .env 
```
NEXT_PUBLIC_USER_LIST = https://www.melivecode.com/api/users
NEXT_PUBLIC_USER_SEARCH = https://www.melivecode.com/api/users?search=karn
NEXT_PUBLIC_PER_PAGE = https://www.melivecode.com/api/users?page=1&per_page=10
NEXT_PUBLIC_SORT = https://www.melivecode.com/api/users?sort_column=id&sort_order=desc
NEXT_PUBLIC_USER_CREATE = https://www.melivecode.com/api/users/create
NEXT_PUBLIC_USER_UPDATE = https://www.melivecode.com/api/users/update
NEXT_PUBLIC_USER_DELETE = https://www.melivecode.com/api/users/delete
NEXT_PUBLIC_ATTRACTION_LIST = https://www.melivecode.com/api/attractions
NEXT_PUBLIC_ATTRACTION_SEARCH = https://www.melivecode.com/api/attractions?search=island
NEXT_PUBLIC_ATTRACTION_PAGE = https://www.melivecode.com/api/attractions?page=1&per_page=10
NEXT_PUBLIC_ATTRACTION_SORT = https://www.melivecode.com/api/attractions?sort_column=id&sort_order=desc
NEXT_PUBLIC_ATTRACTION_SEARCH_PAGE_SORT = https://www.melivecode.com/api/attractions?search=island&page=1&per_page=10&sort_column=id&sort_order=desc
NEXT_PUBLIC_ATTRACTION_LANGUAGE = https://www.melivecode.com/api/th/attractions
NEXT_PUBLIC_ATTRACTION_DETAIL = https://www.melivecode.com/api/attractions/1
NEXT_PUBLIC_ATTRACTION_LANGUAGE_DETAIL = https://www.melivecode.com/api/th/attractions/1
NEXT_PUBLIC_ATTRACTION_STATIC_PATH = https://www.melivecode.com/api/attractions/static_paths
NEXT_PUBLIC_PET_SALES_LIST = https://www.melivecode.com/api/pets/7days/2023-01-01
NEXT_PUBLIC_PET_SALES_DAILY_LIST = https://www.melivecode.com/api/pets/2023-01-01

```
- return form สำหรับเพิ่ม User
```
<Box
    component="form"
    noValidate
    onSubmit={handleSubmit}>
    <Typography component="div">create user</Typography>
    <TextField label="firstname" onChange={e => handleInputChange('fname', e.target.value)}/>
    <TextField label="lastname" onChange={e => handleInputChange('lname', e.target.value)} />
    <TextField label="username" onChange={e => handleInputChange('username', e.target.value)} />
    <TextField label="email" onChange={e => handleInputChange('email', e.target.value)} />
    <Button type="submit">submit</Button>
</Box>
```
- return รายชื่อ User
```
{userData != null && (
    <ol>
        {userData.map((item) => (
            item.id > 12 && (
            <li key={item.id}>{item.fname}  {item.lname}
                <Button onClick={() => handleClickOpen(item)}>แก้ไข</Button>
                <Button onClick={() => handleOpenDelete(item.id)}>ลบ</Button>
            </li>)))}
    </ol>
)}
```
- return ลบ
```
<Dialog
    open={openDelete}
    onClose={handleCloseDelete}
    aria-labelledby="alert-delete-title"
    aria-describedby="alert-delete-description">
        <DialogTitle id="alert-delete-title">{"ลบ"}</DialogTitle>
        <DialogContent>
            <DialogContentText id="alert-dialog-description">
                ต้องการลบหรือไม่
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={handleCloseDelete}>ยกเลิก</Button>
            <Button onClick={() => handleDelete(idToDelete)} autoFocus>ลบ</Button>
        </DialogActions>
</Dialog>
```
- return แก้ไข
```
<Dialog open={open} onClose={handleClose}>
    <DialogTitle>update user</DialogTitle>
    <DialogContent>
        <TextField
            autoFocus
            margin="dense"
            id="firstname"
            label="firstname"
            type="text"
            value={userUpdate.fname}
            onChange={e => handleUpdateChange('fname', e.target.value)}/>
        <TextField
            autoFocus
            margin="dense"
            id="lastname"
            label="lastname"
            type="text"
            value={userUpdate.lname}
            onChange={e => handleUpdateChange('lname', e.target.value)}/>
        <TextField
            autoFocus
            margin="dense"
            id="username"
            label="username"
            type="text"
            value={userUpdate.username}
            onChange={e => handleUpdateChange('username', e.target.value)}/>
    </DialogContent>
    <DialogActions>
        <Button onClick={handleClose}>cancel</Button>
        <Button onClick={handleSubmitUpdate}>update</Button>
    </DialogActions>
</Dialog>

```

