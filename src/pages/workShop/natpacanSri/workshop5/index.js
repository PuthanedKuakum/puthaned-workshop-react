import React, { useState } from 'react'
import {
  Container,
  Grid,
  Box,
  Typography,
  Button,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Tab,
  Tabs
  // TabList,
  // TabContext

} from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

//* import components
import Accordion_ from 'src/components/johnny/workshop5/accordion';
import { TabPanel } from 'src/components/freshy/workshop'
import Layout from 'src/components/johnny/workshop5/layout'
import UserList from 'src/components/johnny/workshop5/user/userlist'
import Search from 'src/components/johnny/workshop5/user/search'
import Pagination from 'src/components/johnny/workshop5/user/pagination';
import Sort from "src/components/johnny/workshop5/user/sort"
import SPS from "src/components/johnny/workshop5/user/sps"
import UserDetail from 'src/components/johnny/workshop5/user/userDetail'
import CreateUser from 'src/components/johnny/workshop5/user/createUser'
import UpdateUser from 'src/components/johnny/workshop5/user/updateUser'
import DeleteUser from 'src/components/johnny/workshop5/user/deleteUser'
//*
import AttractList from 'src/components/johnny/workshop5/attraction/attractList';
import AttrSearch from 'src/components/johnny/workshop5/attraction/Search';
import AttrPagination from 'src/components/johnny/workshop5/attraction/pagination'
import AttrSort from 'src/components/johnny/workshop5/attraction/sort'
import AttrSps from 'src/components/johnny/workshop5/attraction/sps'




export default function index() {

  const accordionUser = [
    {
      name: 'User List',
      api: 'api/users',
      component: <UserList />
    },
    {
      name: 'Search',
      api: 'api/users?search=search',
      component: <Search />
    },
    {
      name: 'Pagination',
      api: 'api/users?page={page}&per_page={per_page}',
      component: <Pagination />
    },
    {
      name: 'Sort',
      api: 'api/users?sort column={sort_column}&sort_order={sort_order}',
      component: <Sort />
    },
    {
      name: `Search + Pagination + Sort`,
      api: 'api/users?search={search}&page={page}&per_page={per_page}&sort_column={sort_column}&sort_order={sort_order}',
      component: <SPS />
    },
    {
      name: 'User Detail',
      api: 'api/users/{id}',
      component: <UserDetail />
    },
    {
      name: 'Create User',
      api: 'api/users/create',
      component: <CreateUser />
    },
    {
      name: 'Update User',
      api: 'api/users/update',
      component: <UpdateUser />
    },
    {
      name: 'Delete User',
      api: 'api/users/delete',
      component: <DeleteUser />
    },
  ]

  const accordionAttractions = [
    {
      name: 'Attraction List',
      api: 'api/attractions',
      component: <AttractList />
    },
    {
      name: 'Attraction Search',
      api: 'api/attractions?search=search',
      component: <AttrSearch />
    },
    {
      name: 'Attraction Pagination',
      api: 'api/attractions?page={page}&per_page={per_page}',
      component: <AttrPagination />
    },
    {
      name: 'Attraction Sort',
      api: 'api/attractions?sort column={sort_column}&sort order={sort_order}',
      component: <AttrSort />
    },
    {
      name: 'Attraction Search + Pagination  + Sort ',
      api: 'api/attractions?sort column={sort_column}&sort order={sort_order}',
      component: <AttrSort />
    },
  ]

  const [expanded, setExpanded] = useState(false);
  const [value, setValue] = useState(0);

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
    console.log(value)
  };

  const handleChangeAccordion = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <Layout>
      <Container maxWidth="md" sx={{ marginTop: "30px" }}>
        {/* <Typography variant='h3' sx={{ marginBottom: '30px' }} >USERS</Typography> */}

        <Tabs
          value={value}
          onChange={handleChangeTab}
          variant="scrollable"
          scrollButtons="auto"
          allowScrollButtonsMobile
          aria-label="Workshop List"
          className='mb-8'
          indicatorColor="black"
        >

          <Tab label='USERS' disableRipple
            sx={{
              color: 'black',
              fontSize: '1.3em',
              fontWeight: '600',
              color: 'ligthgray',
              '&.Mui-selected': {
                fontSize: '1.7em',
                fontWeight: '700',
                borderBottom: '3px solid black',
                color: 'black'
              },
            }}
          />
          <Tab label='ATTRACTIONS' disableRipple
            sx={{
              color: 'black',
              fontSize: '1.3em',
              fontWeight: '600',
              color: 'ligthgray',
              '&.Mui-selected': {
                fontSize: '1.7em',
                fontWeight: '700',
                borderBottom: '3px solid black',
                color: 'black'
              },
            }}
          />
        </Tabs>

        <Box sx={{ border: '1.5px solid #f2f4f7', borderRadius: '8px', boxShadow: 3, marginBottom: '50px' }} >

          <TabPanel value={value} index={0}>

            {accordionUser.map((item, index) => (
              <Accordion key={index} expanded={expanded === `panel${index + 1}`} onChange={handleChangeAccordion(`panel${index + 1}`)}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls={`panel${index + 1}bh-content`}
                  id={`panel${index + 1}bh-header`}
                >
                  <Typography sx={{ width: '20%', flexShrink: 0, fontWeight: '600', marginRight: '10px' }}>
                    {item.name}
                  </Typography>
                  <Typography sx={{ widht: "80%", color: 'text.secondary' }}>
                    {item.api}
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    {expanded ? item.component : ''}
                  </Typography>
                </AccordionDetails>
              </Accordion>

            ))}

          </TabPanel>

          <TabPanel value={value} index={1}>
            {accordionAttractions.map((item, index) => (
              <Accordion key={index} expanded={expanded === `panel${index + 1}`} onChange={handleChangeAccordion(`panel${index + 1}`)}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls={`panel${index + 1}bh-content`}
                  id={`panel${index + 1}bh-header`}
                >
                  <Typography sx={{ width: '20%', flexShrink: 0, fontWeight: '600', marginRight: '10px' }}>
                    {item.name}
                  </Typography>
                  <Typography sx={{ widht: "80%", color: 'text.secondary' }}>
                    {item.api}
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    {expanded ? item.component : ''}
                  </Typography>
                </AccordionDetails>
              </Accordion>
            ))}
          </TabPanel>

        </Box>
      </Container>
    </Layout>
  )
}
