import React from 'react'
import Image from 'next/image'
import {
  Container,
  Box,
  Typography,
  Button,
  Grid,
} from "@mui/material"

//* import components
import Layout from './layout'
import HeroSection from 'src/components/johnny/workshop2/heroSection'
import Campaign from 'src/components/johnny/workshop2/campaign'
import BlogSection from 'src/components/johnny/workshop2/blogSection'



export default function () {

  // const logoHomePic1 = "https://img.freepik.com/free-photo/3d-view-house-model_23-2150761168.jpg?t=st=1700474521~exp=1700478121~hmac=9c5fedd5622eb305496bde7bc088162638c5d2bde6668793c125914301b4b520&w=740"

  return (
    <Layout>
      <HeroSection/>
      <Campaign/>
      <BlogSection/>
    </Layout>
  )
}
