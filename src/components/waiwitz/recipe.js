import { Grid, Box, Card, CardHeader, CardContent, Container, Typography, ImageList, ImageListItem, Paper } from "@mui/material";
import styles from "src/styles/waiwitzStyles.module.css";

const Repice = () => {
    return (
        <>
            <Container maxWidth='xl' className={styles.spaceContainer}>
                <Grid item xs={7}>
                    <Card>
                        <Typography variant="h5" style={{ padding: '1em 0 0 1em' }}>
                            สูตรไข่เจียวฟู 🧚‍♀️
                        </Typography>
                        <CardContent>
                            <Typography>
                                ไข่เจียวธรรมดาโลกไม่จำ ขอเสนอ ไข่เจียวฟู โดยเราจะเจียวไข่สด ตราซีพีให้ฟู แล้วนำไปเทผ่านตะแกรงลงน้ำมันร้อน ๆ ตัวไข่จะฟูกรอบน่ารับประทาน เสิร์ฟพร้อมซอสพริก
                            </Typography>
                            <Grid container spacing={2} justifyContent="center" style={{ marginTop: '1em' }} columns={{ xs: 4, sm: 8, md: 12 }}>
                                <Grid xs={3} item style={{ marginRight: '2em' }} >
                                    <ImageList cols={1} sx={{ width: '260px' }}>
                                        {img.map((i) => (
                                            <Paper elevation={3} key={i.img}>
                                                <ImageListItem>
                                                    <img src={i.img} alt="ไข่เจี๊ยว" style={{borderRadius: '10px', boxShadow: '0 0 10px #ddd'}}/>
                                                </ImageListItem>
                                            </Paper>
                                        ))}
                                    </ImageList>
                                </Grid>
                                <Grid xs={8} item >
                                    <Card className={styles.spaceContainer}>
                                        <CardHeader title='ส่วนผสม' />
                                        <CardContent style={{ paddingTop: '10px' }}>
                                            <ul style={{ listStyle: 'none', padding: '0' }}>
                                                <li className={styles.material}>
                                                    <span>ไข่ไก่สด ตราซีพี</span>
                                                    <span className={styles.material_size}>3 ฟอง</span></li>
                                                <li className={styles.material}>
                                                    <span>ซอสหอยนางรม (ช้อนโต๊ะ)</span>
                                                    <span className={styles.material_size}>1 ช้อนโต๊ะ</span></li>
                                                <li className={styles.material}>
                                                    <span>
                                                        น้ำปลา
                                                    </span>
                                                    <span className={styles.material_size}>1/2ช้อนโต๊ะ</span></li>
                                                <li className={styles.material}>
                                                    <span>
                                                        ต้นหอมซอย (ช้อนโต๊ะ)
                                                    </span>
                                                    <span className={styles.material_size}>1 ช้อนโต๊ะ</span></li>
                                                <li>
                                                    <span>น้ำมันพืช 2 สำหรับทอดและผัดซอส</span>
                                                </li>
                                            </ul>

                                        </CardContent>
                                    </Card>
                                    <Card>
                                        <CardHeader title='วิธีทำ' />
                                        <CardContent style={{ paddingTop: '0px' }}>
                                            <ul style={{ listStyle: 'none', padding: '0' }}>
                                                <li style={{ marginBottom: '5px' }}>ตีไข่ : ตอกไข่ลงไปในถ้วยที่เตรียมไว้ ใส่น้ำปลา ซอสหอยนางรม ตีให้เข้ากัน</li>
                                                <li style={{ marginBottom: '5px' }}>ทอดไข่ : ตั้งกระทะไฟกลาง เทน้ำมันพืชลงไป รอจนน้ำมันเดือด เทไข่ที่ปรุงไว้ลงไปผ่านกระชอน ทอดจนเหลืองกรอบ</li>
                                                <li>จัดเสิร์ฟ : ตักใส่จานที่เตรียมไว้ โรยต้นหอมซอย พร้อมเสิร์ฟ</li>
                                            </ul>
                                        </CardContent>
                                    </Card>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
            </Container>
        </>
    )
}

const img = [{
    img: 'https://s359.kapook.com/pagebuilder/6fa89382-ebdf-4c20-a33d-e6d997829266.jpg'
},
{
    img: 'https://assets.unileversolutions.com/recipes-v2/117591.jpg'
},
];

export default Repice;