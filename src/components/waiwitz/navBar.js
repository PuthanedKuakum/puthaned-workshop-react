import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { MenuItem } from '@mui/material';
import Link from 'next/link';
import styles from "src/styles/waiwitzStyles.module.css";

export default function NavBar() {
  return (
      <AppBar position="static" className={styles.navBar}>
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <b className={styles.logo}>My Resume</b>
          </Typography>
          <Link href="resumeAboutMe" className={styles.navMenuLink}>
            <MenuItem>
              About ME!
            </MenuItem>
          </Link>
          <Link href="resumeProject" className={styles.navMenuLink}>
            <MenuItem>
              Project
            </MenuItem>
          </Link>
        </Toolbar>
      </AppBar>
  );
}