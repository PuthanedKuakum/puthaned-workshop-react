import { Box, Grid, Typography } from "@mui/material";
import CheckCircleRoundedIcon from '@mui/icons-material/CheckCircleRounded';
import styles from "src/styles/waiwitzStyles.module.css";
import ListText from "./listText";


const Introduce1 = ({ head1, head2, body }) => {
    return (
        <>
            <Grid container columns={{ xs: 4, sm: 8, md: 12 }} justifyContent={'center'}>
                <Grid item xs={6}>
                    <Box sx={{ background: '#C4C4C4', width: '100%', height: '500px' }}></Box>
                </Grid>
                <Grid item xs={6}>
                    <Box sx={{ padding: '20px' }}>
                        <Typography sx={{ fontWeight: 'bold', color: '#757575' }}>{head1}</Typography>
                        <Typography variant="h3" sx={{ padding: '15px 0' }} gutterBottom>
                            {head2}
                        </Typography>
                        <Typography variant="body1" gutterBottom>
                            {body}
                        </Typography>
                        <ListText/>
                        <Grid container justifyContent="space-evenly" sx={{ marginTop: '2em' }}>
                            <Grid item>
                                <Typography variant="h2" className={styles.HeadContentStat}>
                                    15Y
                                </Typography>
                                <Typography variant="p" className={styles.textContentStat}>
                                    Experience
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="h2" className={styles.HeadContentStat}>
                                    25+
                                </Typography>
                                <Typography variant="p" className={styles.textContentStat}>
                                    Best Team
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="h2" className={styles.HeadContentStat}>
                                    500+
                                </Typography>
                                <Typography variant="p" className={styles.textContentStat}>
                                    Total Client
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
            </Grid>
        </>
    )
}

export default Introduce1;