import React from "react";
import { Grid, Paper, Typography, Card, CardMedia } from "@mui/material";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import Footer2 from 'src/components/pang/footer3';
import Footer3 from 'src/components/pang/footer2';
import Footer4 from 'src/components/pang/footer4';




const BodyPortfolio2 = () => {
  return (
    <Grid container spacing={2} justifyContent="space-around" paddingTop="50px" paddingLeft="30px">
      {/* First Box with YouTube Link */}
      <Grid item xs={12} sm={6} md={6} lg={6}>
        <Paper elevation={3} style={{ width: "100%", height: "400px", padding: "20px" }}>
        <iframe width="700" height="370" src="https://www.youtube.com/embed/NITw4Cums0s?list=RDmt2zvsvkKKo" title="Doja Cat - Agora Hills" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
        </Paper>
      </Grid>
      

      {/* Second Box with Title, Details, and Text */}
      <Grid item xs={12} sm={6} md={6} lg={6}>
        <Paper elevation={3} style={{ width: "100%", height: "400px", padding: "20px" }}>
          <Typography variant="h6" gutterBottom style={{ paddingTop: "10px" }}>
            TEAM WORKING
          </Typography>
          <Typography
            variant="h4"
            gutterBottom
            style={{
              fontWeight: "bold",
              color: "#000000",
              letterSpacing: "2px",
              paddingTop: "20px",
            }}
          >
            LETS SEE OUR PROJECT
          </Typography>
          <Typography
            variant="h4"
            gutterBottom
            style={{ fontWeight: "bold", color: "#000000", letterSpacing: "2px" }}
          >
            TEAM WORKER
          </Typography>
          <Typography
            style={{
              fontSize: "16px",
              color: "#000000",
              paddingTop: "20px",
            }}
          >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec
            ullamcorper mattis, pulvinar dapibus leo.
          </Typography>

          <Typography style={{paddingTop: "15px", marginLeft: "20px"}}>
          <CheckCircleIcon>
            </CheckCircleIcon> Flexible Time
            </Typography>
            <Typography style={{paddingTop: "15px", marginLeft: "20px"}}>
          <CheckCircleIcon>
            </CheckCircleIcon> Perfect Work
            </Typography>
            <Typography style={{paddingTop: "15px", marginLeft: "20px"}}>
          <CheckCircleIcon>
            </CheckCircleIcon> Client Priority
            </Typography>
        </Paper>
      </Grid>
    </Grid>





  );
};

export default BodyPortfolio2;
