import React from 'react'
import {
    Container,
    Typography,
} from '@mui/material';

export default function cLayout({ title,children }) {
    return (
        <Container maxWidth="md">
            <Typography variant="h5" marginBottom={3}>
                {title}
            </Typography>
            {children}
        </Container>
    )
}
