import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #e5e7eb',
  borderRadius:'10px',
  boxShadow: 24,
  p: 4,
};

export default function UserModal({ userData, open, handleClose }) {
    return (
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
        <img src={userData.avatar} alt="Avatar" style={{ width: '150px',height:'150px',margin: 'auto', marginBottom: '10px' }} />
          <Typography id="modal-modal-title" variant="h5" component="h2" sx={{textAlign:'center'}}>
            {userData.fname} {userData.lname}
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <strong>Email:</strong> {userData.email}
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <strong>Username:</strong> {userData.username}
          </Typography>
          
        </Box>
      </Modal>
    );
  }
