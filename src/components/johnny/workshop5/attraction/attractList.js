import React, { useState, useEffect } from 'react';
import {
    Container,
    Typography,
    CircularProgress
} from '@mui/material';
import CLayout from '../cLayout';
import AttrTable from '../attrTable';

export default function AttractList() {
    const [attrData, setAttrData] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch("https://www.melivecode.com/api/attractions");
                const data = await response.json();
                setAttrData(data);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching data:', error);
                setLoading(false);
            }
        };

        fetchData();
    }, []); // Remove 'loading' from the dependency array

    return (
        <CLayout title='Attraction List'>
            {loading ? (
                <CircularProgress />
            ) : (
                <AttrTable attrData={attrData} />
            )}
        </CLayout>
    );
}
