import React, { useState, useEffect } from 'react';
import { Container, Typography, CircularProgress } from '@mui/material';
import CLayout from '../cLayout';
import UserTable from '../userTable';

export default function sps() {

    const [userData, setUserData] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/users?search=ka&page=1&per_page=10&sort_column=id&sort_order=desc`);
            const res = await response.json();
            setUserData(res.data);
            // console.log("search+pagination+sort",res)            
            // console.log("userData:",userData)

            if(userData.length > 0){
                    setLoading(false);
                }

          } catch (error) {
            console.error('Error fetching data:', error);
            setLoading(false);
          }
        };
    
        fetchData();
      }, [userData]);

  return (
    <CLayout title="Search+Pagination+Sort">
        {loading ? (
                <CircularProgress />
            ) : (
                <>
                    <UserTable userData={userData} />
                </>
            )}
    </CLayout>
  )
}
