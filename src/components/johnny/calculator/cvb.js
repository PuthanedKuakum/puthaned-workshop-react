import React, { useState, useEffect } from 'react';

export default function cvb() {
    const [bases, setBases] = useState([
        { id: "2", name: 'Binary(base2)', check: true, value: '' },
        { id: "8", name: 'Octal(base 8)', check: false, value: '' },
        { id: "10", name: 'Decimal(base 10)', check: true, value: '' },
        { id: "16", name: 'Hexadecimal(base 16)', check: false, value: '' },
    ]);

    const handleCheckboxChange = (index) => {
        setBases((prevBases) => {
            const newBases = [...prevBases];
            newBases[index] = { ...newBases[index], check: !newBases[index].check };
            return newBases;
        });
    };

    const handleInputChange = (index, inputValue) => {
        setBases((prevBases) => {
            const newBases = [...prevBases];
            newBases[index] = { ...newBases[index], value: inputValue };
            return newBases;
        });
    };


    const clear = () => {
        const clearedBases = bases.map(base => ({ ...base, value: '' }));
        setBases(clearedBases);
    };


    const handleChange = (index, newValue) => {
        // Create a copy of the bases array
        const updatedBases = [...bases];

        // Update the value of the specific base at the given index
        updatedBases[index] = { ...updatedBases[index], value: newValue };

        // Update the state with the new array of bases
        setBases(updatedBases);
        console.log(bases)
    };


    const convertFromDecimal = (num, base) => {
        if (isNaN(num)) {
            return "";
        } else {
            return num.toString(base);
        }
    }

    const convertBase = () => {

        setBases(prevBases => {
            return prevBases.map(base => {
                if (base.value !== '' && base.check) {
                    const decimalValue = parseInt(base.value, base.id);
                    // Create a copy of the bases array
                    const updatedBases = [...prevBases];
                    console.log([...prevBases])

                    // Update the values of other bases
                    updatedBases.forEach((otherBase, index) => {
                        if (otherBase.value === '') {

                            updatedBases[index] = {
                                ...otherBase,
                                value: convertFromDecimal(decimalValue, otherBase.id),
                            };

                            updatedBases[index] = { ...updatedBases[index], value: convertFromDecimal(decimalValue, otherBase.id) };

                            setBases(updatedBases);

                        }
                    });

                    return {
                        ...base,
                        value: convertFromDecimal(decimalValue, base.id),
                    };
                }
                return base;
            });
        });
    }

    // const convertBase = () => {

    //     setBases(prevBases => {
    //         return prevBases.map(base => {
    //             if (base.value !== '' && base.check) {
    //                 const decimalValue = parseInt(base.value, base.id);
    //                 // Create a copy of the bases array
    //                 const updatedBases = [...prevBases];
    //                 console.log([...prevBases])


    //                 // Update the values of other bases
    //                 updatedBases.forEach((otherBase, index) => {
    //                     if (otherBase.value === '') {

    //                         updatedBases[index] = {
    //                             ...otherBase,
    //                             value: convertFromDecimal(decimalValue, otherBase.id),
    //                         };

    //                         updatedBases[index] = { ...updatedBases[index], value: convertFromDecimal(decimalValue, otherBase.id) };

    //                         setBases(updatedBases);

    //                     }
    //                 });

    //                 return {
    //                     ...base,
    //                     value: convertFromDecimal(decimalValue, base.id),
    //                 };
    //             }



    //             return base;
    //         });
    //     });
    // }




    return (
        <div className="bg-white min-w-fit m-auto rounded-2xl overflow-hidden shadow-xl p-5">
            <ul className="items-center w-full text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg flex mb-5">
                {bases.map((base, index) => (
                    <li className="border-b border-gray-200 sm:border-b-0 sm:border-r px-2" key={index}>
                        <div className="flex items-center ps-3">
                            <input
                                id={base.name}
                                type="checkbox"
                                checked={base.check}
                                onChange={() => handleCheckboxChange(index)}
                                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500"
                            />
                            <label htmlFor={base.name} className="w-full py-3 ms-2 text-sm font-medium text-gray-900">
                                {base.name}
                            </label>
                        </div>
                    </li>
                ))}
            </ul>

            <form className="overflow-hidden rounded-xl border border-gray-300">
                {bases.map((base, index) => (
                    base.check && (
                        <div className="flex" key={index}>
                            <label className="inline-flex w-[11rem] items-center px-3 text-sm text-gray-900 bg-gray-200 border-b border-gray-300">
                                {base.name}
                            </label>
                            <input
                                type="text"
                                id={base.id}
                                value={base.value}
                                onChange={(e) => handleChange(index, e.target.value)}
                                className="rounded-none bg-gray-50 border-b border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5"
                            ></input>
                            {/* <p>{base.value}</p> */}
                        </div>
                    )
                ))}
            </form>

            <button
                type="button"
                onClick={convertBase}
                className="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mt-5 mr-5"
            >
                Convert Base
            </button>
            <button
                onClick={clear}
                className="relative inline-flex items-center justify-center p-0.5 mb-2 me-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-pink-500 to-orange-400 group-hover:from-pink-500 group-hover:to-orange-400 hover:text-whitefocus:ring-4 focus:outline-none focus:ring-pink-200 ">
                <span className="relative px-5 py-2.5 transition-all ease-in duration-75 bg-white rounded-md group-hover:bg-opacity-0">
                    Clear
                </span>
            </button>
            
            {/* {bases.map((base) => (
                <div className='block'>&#123;
                    <div className='block'>
                        "id":{base.id},
                        "name":{base.name},
                        "check":{base.check},
                        "value":{base.value}
                    </div>
                    &#125;,
                </div>
            ))} */}
        </div>
    );
}
