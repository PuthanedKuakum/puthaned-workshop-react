import React, { useState,useEffect } from 'react';
import { AppBar, Toolbar, Typography, Button, IconButton, Badge } from '@mui/material';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import Cart from "./cart"

const Navbar = () => {
  
  const [item ,setItem] = useState([])

  useEffect(() => {
    // Retrieve cart items from localStorage when component mounts
    const storedCart = JSON.parse(localStorage.getItem('cartItems')) || [];
    setItem(storedCart);
  }, []);

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          Johnnnny's store
        </Typography>
        <Button color="inherit">Home</Button>
        <Button color="inherit">Products</Button>
        <Button color="inherit">Contact</Button>
        {/* <IconButton color="inherit" sx={{border:'1px solid'}}>
          <Badge badgeContent={4} color="error" sx={{padding:'0',borderRadius:'none'}}>
            <Cart/>
          </Badge>
        </IconButton> */}
        <Badge badgeContent={item.length} color="error" sx={{padding:'0',borderRadius:'none',}}>
            <Cart />
        </Badge>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
