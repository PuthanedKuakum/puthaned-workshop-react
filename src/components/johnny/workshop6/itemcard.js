import React, { useState, useEffect } from 'react';
import { Card, CardContent, CardMedia, Typography, Button, Box } from '@mui/material';

const ItemCard = ({ product, onAddToCart }) => {
  const { id, title, description, image, price } = product;

  const handleAddToCart = () => {
    onAddToCart({
      id,
      title,
      description,
      image,
      price,
    });
  };

  return (
    <Card>
      <CardMedia
        component="img"
        image={image}
        alt={title}
        sx={{
          maxHeight: '400px',
          height: '300px',
          objectFit: 'contain',
          padding:'50px'
        }}
      />
      <CardContent sx={{ borderTop: '1px solid #e5e7eb', paddingTop: '10px', paddingBottom: '0' }}>
        <Typography variant="h5" noWrap component="div" sx={{ marginBottom: '4px' }}>
          {title}
        </Typography>
        <Typography
          variant="body2"
          color="text.secondary"
          noWrap
          component="div"
          sx={{ maxHeight: 40, overflow: 'hidden', marginBottom: '10px' }}
        >
          {description}
        </Typography>
        <Box sx={{ display: 'flex', marginBottom: '-10px' }}>
          <Typography variant="h6" color="text.primary" sx={{ margin: 'auto 0' }}>
            {price}$
          </Typography>
          <Button 
          onClick={handleAddToCart}
          variant="contained" color="primary" 
          sx={{ marginLeft: 'auto' }}>
            Add to Cart
          </Button>
        </Box>
      </CardContent>
    </Card>
  );
};

export default ItemCard;
