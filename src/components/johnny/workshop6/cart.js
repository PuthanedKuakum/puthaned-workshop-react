import React, { useState, useEffect } from 'react';
import {
  Box,
  Drawer,
  Button,
  List,
  Divider,
  ListItem,
  Typography,
  Checkbox,
  FormControlLabel,
} from '@mui/material';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import CartItem from './cartItem';

export default function Cart() {
  const [items, setItems] = useState([]);
  const [checkedItems, setCheckedItems] = useState([]);
  const [checkAll, setCheckAll] = useState(false);
  const [state, setState] = React.useState({
    right: false,
  });

  useEffect(() => {
    // Retrieve cart items from localStorage when component mounts
    const storedCart = JSON.parse(localStorage.getItem('cartItems')) || [];
    setItems(storedCart);
  }, [state,items]);

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const handleCheckboxChange = (index) => () => {
    // Toggle the checked status of the item at the given index
    const newCheckedItems = [...checkedItems];
    newCheckedItems[index] = !newCheckedItems[index];
    setCheckedItems(newCheckedItems);
  };

  const handleCheckAllChange = () => {
    // Toggle the "Check All" status
    setCheckAll(!checkAll);
    // Set all individual items to checked or unchecked based on "Check All" status
    setCheckedItems(new Array(items.length).fill(!checkAll));
  };

  const clearItems = () => {
    // Clear items from local storage and state
    localStorage.removeItem('cartItems');
    setItems([]);
    setCheckedItems([]);
  };

  const getTotalPrice = () => {
    // Calculate the total price of checked items
    return items.reduce((total, item, index) => {
      if (checkedItems[index]) {
        return total + item.totalPrice; // Assuming each item has a "price" property
      }
      return total;
    }, 0);
  };

  const list = (anchor) => (
    <Box
      sx={{
        width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 500,
        padding: '20px',
      }}
      role="presentation"
    >
      <Box sx={{ display: 'flex', marginBottom: '20px' }}>
        <Typography variant="h3">Cart</Typography>
        <Button onClick={clearItems} sx={{ marginLeft: 'auto' }}>
          Clear Item
        </Button>
      </Box>

      {items.length > 0 ? (
        items.map((item, index) => (
          <Box key={index} sx={{ display: 'flex' }}>
            <Checkbox
              checked={checkedItems[index] || false}
              onChange={handleCheckboxChange(index)}
              sx={{ margin: 'auto 0' }}
            />
            <CartItem key={index} product={item} />
          </Box>
        ))
      ) : (
        <></>
      )}

      <Box
        sx={{
          borderTop: '1px solid #e5e7eb',
          height: '13%',
          position: 'fixed',
          bottom: 0,
          right: 0,
          width: '500px',
          display: 'flex',
          backgroundColor: 'white',
        }}
      >
        <FormControlLabel
           control={<Checkbox checked={checkAll} onChange={handleCheckAllChange} />}
          label="Check All"
          sx={{ margin: 'auto 0' }}
        />
        <Box
          sx={{
            margin: 'auto 0',
            marginLeft: 'auto',
            marginRight: '10px',
            display: 'flex',
          }}
        >
          <Typography
            sx={{
              margin: 'auto 0',
              marginRight: '10px',
              fontWeight: '600',
            }}
          >
            ${getTotalPrice().toFixed(2)}
          </Typography>
          <Button variant="contained">Purchase</Button>
        </Box>
      </Box>
    </Box>
  );

  return (
    <div>
      {['right'].map((anchor) => (
        <React.Fragment key={anchor}>
          <ShoppingCartIcon
            onClick={toggleDrawer(anchor, true)}
            sx={{ cursor: 'pointer' }}
          />
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
