import React, { useState } from "react";
import { Button, Grid, TextField } from "@mui/material";

const Calculator = () => {
  const [input, setInput] = useState("");

  const handleButtonClick = (value) => {
    setInput((prevInput) => prevInput + value);
  };

  const handleClear = () => {
    setInput("");
  };

  const handleCalculate = () => {
    try {
      setInput(eval(input).toString());
    } catch (error) {
      setInput("Error");
    }
  };

  return (
    <div className="flex flex-col items-center justify-center calculator-container">
      <input type="text" value={input} readOnly className="border mb-2 p-1 text-center bg-blue-100" />
      <div className="grid grid-cols-4 gap-1">
        <button
          onClick={() => handleButtonClick("7")}
          className="bg-blue-500 text-white p-1 w-12   rounded-md"
        >
          7
        </button>
        <button
          onClick={() => handleButtonClick("8")}
          className="bg-blue-500 text-white p-1  w-12 rounded-md"
        >
          8
        </button>
        <button
          onClick={() => handleButtonClick("9")}
          className="bg-blue-500 text-white p-1  w-12 rounded-md"
        >
          9
        </button>
        <button
          onClick={() => handleButtonClick("/")}
          className="bg-yellow-500 text-white p-1  w-12 rounded-md"
        >
          /
        </button>

        <button
          onClick={() => handleButtonClick("4")}
          className="bg-blue-500 text-white p-1  w-12 rounded-md"
        >
          4
        </button>
        <button
          onClick={() => handleButtonClick("5")}
          className="bg-blue-500 text-white p-1  w-12 rounded-md"
        >
          5
        </button>
        <button
          onClick={() => handleButtonClick("6")}
          className="bg-blue-500 text-white p-1  w-12 rounded-md"
        >
          6
        </button>
        <button
          onClick={() => handleButtonClick("*")}
          className="bg-yellow-500 text-white p-1  w-12 rounded-md"
        >
          *
        </button>

        <button
          onClick={() => handleButtonClick("1")}
          className="bg-blue-500 text-white p-1  w-12 rounded-md"
        >
          1
        </button>
        <button
          onClick={() => handleButtonClick("2")}
          className="bg-blue-500 text-white p-1 w-12  rounded-md"
        >
          2
        </button>
        <button
          onClick={() => handleButtonClick("3")}
          className="bg-blue-500 text-white p-1 w-12 rounded-md"
        >
          3
        </button>
        <button
          onClick={() => handleButtonClick("-")}
          className="bg-yellow-500 text-white p-1 w-12 rounded-md"
        >
          -
        </button>

        <button
          onClick={() => handleButtonClick("0")}
          className="bg-blue-500 text-white p-1  w-12 rounded-md"
        >
          0
        </button>
        <button
          onClick={() => handleButtonClick(".")}
          className="bg-gray-500 text-white p-1  w-12 rounded-md"
        >
          .
        </button>
        <button onClick={handleCalculate} className="bg-green-500 text-white p-1  w-12 rounded-md">
          =
        </button>
        <button
          onClick={() => handleButtonClick("+")}
          className="bg-yellow-500 text-white p-1  w-12 rounded-md"
        >
          +
        </button>
      </div>
      <div className="flex justify-center">
        <button onClick={handleClear} className="bg-red-500 text-white p-1 m-1 w-40  rounded-md">
          C
        </button>
      </div>
    </div>
  );
};

export default Calculator;
