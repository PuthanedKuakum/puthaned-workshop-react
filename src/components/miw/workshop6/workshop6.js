import React, { useState } from "react";
import { Container, Grid, Button, TextField } from "@mui/material";
import Products from "./products";
import Cart from "./cart";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";


const products = [
  { id: 1, name: "Whiskey", price: 100, category: "Liquor" },
  { id: 2, name: "Vodka", price: 300, category: "Liquor" },
  { id: 3, name: "Rum", price: 400, category: "Liquor" },
  { id: 4, name: "Rum", price: 400, category: "Liquor" },
  { id: 5, name: "Rum", price: 400, category: "Liquor" },
  { id: 6, name: "Rum", price: 400, category: "Liquor" },
];

const Workshop6 = () => {
  const [cartItems, setCartItems] = useState([]);
  const [isCartOpen, setIsCartOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedCategory, setSelectedCategory] = useState("All");
  const [minPrice, setMinPrice] = useState("");
  const [maxPrice, setMaxPrice] = useState("");

  const filteredProducts = products.filter((product) => {
    const matchSearch = product.name.toLowerCase().includes(searchTerm.toLowerCase());
    const matchCategory = selectedCategory === "All" || product.category === selectedCategory;
    const matchPrice =
      (!minPrice || product.price >= parseInt(minPrice)) &&
      (!maxPrice || product.price <= parseInt(maxPrice));

    return matchSearch && matchCategory && matchPrice;
  });

  const handleAddToCart = (product) => {
    const existingItem = cartItems.find((item) => item.id === product.id);

    if (existingItem) {
      setCartItems(
        cartItems.map((item) =>
          item.id === product.id ? { ...item, quantity: item.quantity + 1 } : item
        )
      );
    } else {
      setCartItems([...cartItems, { ...product, quantity: 1 }]);
    }
  };

  const handleOpenCart = () => {
    setIsCartOpen(true);
  };

  const handleCloseCart = () => {
    setIsCartOpen(false);
  };

  return (
    <Container>
      <div
        style={{
          marginTop: "10px",
          marginBottom: "10px",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <TextField
          label="Search"
          variant="outlined"
          value={searchTerm}
          color="primary"
          onChange={(e) => setSearchTerm(e.target.value)}
          InputProps={{
            style: {
              color: "white", // สีตัวอักษรที่คุณต้องการ
            },
          }}
        />
        <TextField
          label="Min Price"
          variant="outlined"
          type="number"
          color="primary"
          value={minPrice}
          onChange={(e) => setMinPrice(e.target.value)}
          InputProps={{
            style: {
              color: "white", // สีตัวอักษรที่คุณต้องการ
            },
          }}
          style={{ marginLeft: "10px" }}
        />
        <TextField
          label="Max Price"
          variant="outlined"
          type="number"
          color="primary"
          value={maxPrice}
          onChange={(e) => setMaxPrice(e.target.value)}
          InputProps={{
            style: {
              color: "white", // สีตัวอักษรที่คุณต้องการ
            },
          }}
          style={{ marginLeft: "10px" }}
        />
        <Button
          onClick={handleOpenCart}
          variant="contained"
          color="primary"
          style={{ marginLeft: "10px" }}
        >
          <ShoppingCartIcon /> Open Cart ({cartItems.length})
        </Button>
      </div>

      <Grid container spacing={2} marginY={2}>
        {filteredProducts.map((product) => (
          <Grid item key={product.id} xs={12} sm={6} md={4}>
            <Products product={product} onAddToCart={handleAddToCart} />
          </Grid>
        ))}
      </Grid>
      <Cart cartItems={cartItems} isOpen={isCartOpen} onClose={handleCloseCart} />
      <Cart isOpen={isCartOpen} onClose={handleCloseCart} cartItems={cartItems} />
    </Container>

    // <Container>

    //   <Grid container spacing={2}>

    //     {products.map((product) => (
    //       <Grid item key={product.id} xs={12} sm={6} md={4}>
    //         <Products product={product} onAddToCart={handleAddToCart} />

    //       </Grid>
    //     ))}
    //   </Grid>
    //   <Button onClick={handleOpenCart} variant="contained" color="primary" sx={{ marginY:"3rem" }}>
    //     Open Cart
    //   </Button>
    //   <Cart cartItems={cartItems} isOpen={isCartOpen} onClose={handleCloseCart} />
    // </Container>
  );
};

export default Workshop6;
