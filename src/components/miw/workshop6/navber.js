import { AppBar, Toolbar, Typography, Button } from "@mui/material";

const Navbar = () => {
  return (
    <AppBar position="static" style={{ backgroundColor: "black" }}>
      <Toolbar>
        <img
          src={"https://i.pinimg.com/564x/92/42/c1/9242c1ea1abef55ef826a142e8ecc157.jpg"}
          alt="Liquor Icon"
          style={{ width: "40px", marginRight: "10px" }}
        />{" "}
        {/* เพิ่มรูปภาพ */}
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          Royal Rum
        </Typography>
        <Button color="inherit">Shop</Button>
        <Button color="inherit">Cart</Button>
        <Button color="inherit">Login</Button>
        <Button color="inherit">Sign in</Button>
      </Toolbar>
    </AppBar>
  );
};
export default Navbar;
