// pages/login.js
import { useState } from "react";
import {
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material";

// PET SALES SUMMARY (7 DAYS): api/pets/7days/{date}

const Pet = () => {
  const [userData, setUserData] = useState(null);

  const handleSummarys = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/pets/7days/2023-01-01`, {
        method: "GET",
        headers: {
          // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };
  //   PET SALES (DAILY): /api/pets/{date}
  // https://www.melivecode.com/api/pets/2023-01-01
  const handleDaily = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/pets/2023-01-01`, {
        method: "GET",
        headers: {
          // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  return (
    <Container maxWidth="md">
      <Typography variant="h4" align="center" gutterBottom>
        Pet Sales
      </Typography>

      <Container maxWidth="fixed" sx={{ backgroundColor: "", color: "" }}>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleSummarys}
                sx={{ width: "100%" }}
              >
                Attractions Pet Sales Summary api
              </Button>
            </Grid>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleDaily}
                sx={{ width: "100%" }}
              >
                Attractions Pet Sales Daily api
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Container>
  );
};

export default Pet;
