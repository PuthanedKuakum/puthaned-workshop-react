import { Button, CircularProgress, Typography } from '@mui/material';
import { Box, Container } from '@mui/system';
import Image from 'next/image';
import ZoomInIcon from '@mui/icons-material/ZoomIn';
import ZoomOutIcon from '@mui/icons-material/ZoomOut';
import { Suspense, useEffect, useState } from 'react';
import p01d from 'public/assets/freshy/01d.png';
import p01n from 'public/assets/freshy/01n.png';
import p02d from 'public/assets/freshy/02d.png';
import p03d from 'public/assets/freshy/03d.png';
import p04d from 'public/assets/freshy/04d.png';
import p09d from 'public/assets/freshy/09d.png';
import p10d from 'public/assets/freshy/10d.png';
import p10n from 'public/assets/freshy/10n.png';
import p11d from 'public/assets/freshy/11d.png';
import p13d from 'public/assets/freshy/13d.png';
import p50d from 'public/assets/freshy/50d.png';
import avatar_1 from 'public/assets/freshy/0-50.png'
import avatar_2 from 'public/assets/freshy/51-100.png'
import avatar_3 from 'public/assets/freshy/101-150.png'
import avatar_4 from 'public/assets/freshy/151-200.png'
import avatar_5 from 'public/assets/freshy/201-300.png'
import avatar_6 from 'public/assets/freshy/301-500.png'

const apiKey = 'df43a3f1-23cd-485e-a31a-2ef59b5eced5';

export default function WeatherSense({ locationSetting }) {
    const [data, setData] = useState(null);
    const [result, setResult] = useState(null);
    const [error, setError] = useState(null);
    const [scale, setScale] = useState(0);
    const scaleUp = () => {
        if (scale < 3) {
            setScale(scale + 1)
        }
    }
    const scaleDown = () => {
        if (scale > 0) {
            setScale(scale - 1)
        }
    }

    const mapScale = (scale) => {
        if (scale == 0) return 'scale-100';
        if (scale == 1) return 'scale-110';
        if (scale == 2) return 'scale-125';
        if (scale == 3) return 'scale-150';
    }

    const scaleMargin = (scale) => {
        if (scale == 0) return 'mt-16 mb-10';
        if (scale == 1) return 'mt-24 mb-16';
        if (scale == 2) return 'mt-32 mb-24';
        if (scale == 3) return 'mt-52 mb-36';
    }

    useEffect(() => {
        // Function to fetch data from AirVisual API
        const fetchData = async (latitude, longitude) => {
            const apiUrl = `http://api.airvisual.com/v2/nearest_city?lat=${latitude}&lon=${longitude}&key=${apiKey}`;
            try {
                const response = await fetch(apiUrl);
                const result = await response.json();
                console.log(result);
                setResult(result);
                setData(result.data);
            } catch (error) {
                setError(error.message);
            }
        };

        // Get current location
        const getCurrentLocation = () => {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const { latitude, longitude } = position.coords;
                    fetchData(latitude, longitude);
                },
                (error) => {
                    setError(`Error getting location: ${error.message}`);
                }
            );
        };

        if (locationSetting?.country && locationSetting?.state && locationSetting?.city && locationSetting?.isSearch) {
            // use user setting to call API
            const fetchData = async () => {
                const apiUrl = `http://api.airvisual.com/v2/city?city=${locationSetting.city}&state=${locationSetting.state}&country=${locationSetting.country}&key=${apiKey}`;

                try {
                    const response = await fetch(apiUrl);
                    const result = await response.json();
                    console.log(result);
                    setResult(result);
                    setData(result.data);
                } catch (error) {
                    setError(error.message);
                }
            };

            fetchData();
        } else {
            // use lat and long to call API
            getCurrentLocation();
        }
        console.log(data)
    }, [locationSetting]); // Empty dependency array ensures the effect runs only once on mount

    const formatWindDirection = (degrees) => {
        const directions = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];

        // Calculate the index of the direction based on the degrees
        const index = Math.round(degrees / 45) % 8;

        return directions[index];
    };

    const formatIcon = (iconId) => {
        if (iconId == '01d') return p01d;
        if (iconId == '01n') return p01n;
        if (iconId == '02d') return p02d;
        if (iconId == '02n') return p02n;
        if (iconId == '03d') return p03d;
        if (iconId == '04d') return p04d;
        if (iconId == '09d') return p09d;
        if (iconId == '10d') return p10d;
        if (iconId == '10n') return p10n;
        if (iconId == '11d') return p11d;
        if (iconId == '13d') return p13d;
        if (iconId == '50d') return p50d;
    }

    const formatAirQuality = (aqi) => {
        if (aqi <= 50) return 'GOOD';
        if (aqi <= 100) return 'MODERATE';
        if (aqi <= 150) return 'UNHEALTHY FOR SENSITIVE';
        if (aqi <= 200) return 'UNHEALTHY';
        if (aqi <= 300) return 'VERY UNHEALTHY';
        if (aqi <= 500) return 'HAZARDOUS';

        return 'INVALID AQI'; // Handle cases where AQI is out of expected range
    };

    const formatAirQualitySubtitle = (aqi) => {
        if (aqi <= 50) return 'Breathe deeply and enjoy the fresh, clean air around you.';
        if (aqi <= 100) return 'Most people can breathe easily, but individuals with respiratory issues may experience mild discomfort.';
        if (aqi <= 150) return 'Children, the elderly, and those with respiratory issues may experience breathing difficulties. Limit outdoor activity if possible.';
        if (aqi <= 200) return "Breathing discomfort, especially those with respiratory issues. It's best to stay indoors and avoid physical exertion.";
        if (aqi <= 300) return 'Serious breathing discomfort, people with respiratory issues may severe symptoms. Stay indoors and avoid physical activity.';
        if (aqi <= 500) return 'Outdoor air is hazardous. Stay indoors, wear a mask if going outside, especially if you have respiratory issues';

        return 'INVALID AQI'; // Handle cases where AQI is out of expected range
    };

    const formatAvatar = (aqi) => {
        if (aqi <= 50) return avatar_1;
        if (aqi <= 100) return avatar_2;
        if (aqi <= 150) return avatar_3;
        if (aqi <= 200) return avatar_4;
        if (aqi <= 300) return avatar_5;
        if (aqi <= 500) return avatar_6;

        return avatar_1;
    }

    const formatBgColor = (aqi) => {
        if (aqi <= 50) return 'bg-[#4CD964]';
        if (aqi <= 100) return 'bg-[#FBD405]';
        if (aqi <= 150) return 'bg-[#FF9900]';
        if (aqi <= 200) return 'bg-[#EF574E]';
        if (aqi <= 300) return 'bg-[#8E4296]';
        if (aqi <= 500) return 'bg-[#872E47]';

        return 'bg-[#05A0FA]';
    }

    const getGreeting = () => {
        const currentTime = new Date();
        const currentHour = currentTime.getHours();

        if (currentHour >= 5 && currentHour < 12) {
            return 'Good Morning';
        } else if (currentHour >= 12 && currentHour < 18) {
            return 'Good Afternoon';
        } else if (currentHour >= 18 && currentHour < 21) {
            return 'Good Evening';
        } else {
            return 'Good Night';
        }
    };

    return (
        <Container maxWidth='xl' className={scaleMargin(scale)}>
            <div className={'relative mx-auto mb-16 border-gray-800 bg-gray-800 border-[14px] rounded-[2.5rem] h-[600px] w-[300px] shadow-xl ' + mapScale(scale)}>
                <div className="w-[148px] h-[18px] bg-gray-800 top-0 rounded-b-[1rem] left-1/2 -translate-x-1/2 absolute"></div>
                <div className="h-[46px] w-[3px] bg-gray-800 absolute -start-[17px] top-[124px] rounded-s-lg"></div>
                <div className="h-[46px] w-[3px] bg-gray-800 absolute -start-[17px] top-[178px] rounded-s-lg"></div>
                <div className="h-[64px] w-[3px] bg-gray-800 absolute -end-[17px] top-[142px] rounded-e-lg"></div>
                {data && result.status === 'success' ? (
                    <div className="rounded-[2rem] overflow-hidden w-[272px] h-[572px] bg-white flex flex-col">
                        <Box className={`${formatBgColor(data?.current?.pollution?.aqius)} p-4 pt-16 h-[calc(66%-1.5em)] rounded-bl-3xl`}>
                            <Box className='flex flex-row flex-wrap justify-between items-center mb-6'>
                                <Box>
                                    <Typography variant='caption' color='white'>
                                        {getGreeting()}
                                    </Typography>
                                    <Typography variant='body2' fontWeight='medium' color='white'>
                                        Teerawut Sungkagaro
                                    </Typography>
                                </Box>
                                <Image src={formatIcon(data?.current?.weather?.ic) || p01d} className='h-10 w-auto' />
                            </Box>
                            <Box className='flex flex-row flex-wrap justify-between items-center rounded-2xl bg-gray-100/20 backdrop-filter backdrop-blur-[2px] shadow-md shadow-gray-400/20 border border-gray-100/50 p-4 pb-3 absolute w-[calc(100%-32px)]'>
                                <Box>
                                    <Typography variant='caption' color='white'>
                                        Feel like
                                    </Typography>
                                    <Typography variant='body1' fontWeight='medium' color='white'>
                                        {data?.current?.weather?.tp}°
                                    </Typography>
                                    <Typography variant='caption' color='white'>
                                        Humidity
                                    </Typography>
                                    <Typography variant='body1' fontWeight='medium' color='white'>
                                        {data?.current?.weather?.hu}%
                                    </Typography>
                                    <Typography variant='caption' color='white'>
                                        Pressure
                                    </Typography>
                                    <Typography variant='body1' fontWeight='medium' color='white'>
                                        {data?.current?.weather?.pr} <Typography variant='caption'>hPa</Typography>
                                    </Typography>
                                    <Typography variant='caption' color='white'>
                                        Wind Speed
                                    </Typography>
                                    <Typography variant='body1' sx={{ mb: 3 }} fontWeight='medium' color='white'>
                                        {formatWindDirection(data?.current?.weather?.wd)} {data?.current?.weather?.ws} <Typography variant='caption'>km/s</Typography>
                                    </Typography>
                                    <Typography variant='caption'>
                                        Air is
                                    </Typography>
                                    <Typography variant='body1' fontWeight='medium'>
                                        {formatAirQuality(data?.current?.pollution?.aqius)}
                                    </Typography>
                                    <Typography sx={{ fontSize: 10, mb: 2 }}>
                                        {formatAirQualitySubtitle(data?.current?.pollution?.aqius)}
                                    </Typography>
                                    <hr />
                                    <Box className='flex flex-row flex-wrap justify-between items-center gap-4'>
                                        <Box className='flex flex-row flex-wrap gap-2'>
                                            <Box className='flex flex-col flex-wrap justify-between items-center mt-2'>
                                                <Typography variant='button' sx={{ fontSize: 10 }}>
                                                    US AQI
                                                </Typography>
                                                <Typography variant='body2' fontWeight='medium'>
                                                    {data?.current?.pollution?.aqius}
                                                </Typography>
                                            </Box>
                                            <Box className='flex flex-col flex-wrap justify-between items-center mt-2'>
                                                <Typography variant='button' sx={{ fontSize: 10 }}>
                                                    CN AQI
                                                </Typography>
                                                <Typography variant='body2' fontWeight='medium'>
                                                    {data?.current?.pollution?.aqicn}
                                                </Typography>
                                            </Box>
                                        </Box>
                                        <Box className='flex flex-col flex-wrap justify-between items-end mt-2'>
                                            <Typography variant='button' sx={{ fontSize: 10 }}>
                                                Address
                                            </Typography>
                                            <Typography variant='body2' sx={{ fontSize: 10 }}>
                                                {data?.city}, {data?.country}
                                            </Typography>
                                        </Box>
                                    </Box>
                                </Box>
                                <Image src={formatAvatar(data?.current?.pollution?.aqius)} className='h-auto w-24 object-contain absolute right-3 -translate-y-12' alt='adam' />
                            </Box>
                        </Box>
                    </div>
                ) : data && result.status === 'fail' ? (
                    <div className="rounded-[2rem] overflow-hidden w-[272px] h-[572px] bg-white flex justify-center items-center">
                        <Typography variant='h6'>
                            {result.status.toUpperCase()}: {result.data.message}
                        </Typography>
                    </div>
                ) : (
                    <div className="rounded-[2rem] overflow-hidden w-[272px] h-[572px] bg-white flex justify-center items-center">
                        <CircularProgress />
                    </div>
                )}
                <Box className='flex flex-row gap-4 items-center absolute -left-32 top-56'>
                    <Button variant="outlined" color="primary" onClick={scaleUp}>
                        <ZoomInIcon />
                    </Button>
                </Box>
                <Box className='flex flex-row gap-4 items-center absolute -right-32 top-56'>
                    <Button variant="outlined" color="primary" onClick={scaleDown}>
                        <ZoomOutIcon />
                    </Button>
                </Box>
            </div>
        </Container>
    );
}
