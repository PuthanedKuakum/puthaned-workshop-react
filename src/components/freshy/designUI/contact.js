import { Box, Button, Container, Paper, Typography, Grid } from "@mui/material";
import Image from "next/image";
import pLogo1 from 'public/assets/freshy/designUI/logo-14 1.png';
import pLogo2 from 'public/assets/freshy/designUI/logo-16 1.png';
import pLogo3 from 'public/assets/freshy/designUI/logo-25 1.png';
import pLogo4 from 'public/assets/freshy/designUI/logo-26 1.png';

export default function Contact() {
    return (
        <Paper
            square
            elevation={4}
            sx={{
                pt: { xs: 20, md: 10}, pb: 6, px: 10,
                position: 'absolute',
                top: { xs: '-90%', sm: '-100%', md: '-125%' },
                display: 'flex', flexDirection: 'column',
                alignItems: 'center', justifyContent: 'center',
                bgcolor: '#3e88ec',
                maxLines: { xs: 4, md: 2 },
                mr: 3
            }}>
            <Paper
                elevation={4}
                square
                sx={{
                    position: 'absolute',
                    bgcolor: 'white', px: 4,
                    top: {xs: '-20%', md:'-16%'}, width: '90%',
                    mx: 40, display: 'flex',
                    justifyContent: 'center',
                    gap: 4,
                    overflow: 'hidden'
                }}>
                <Grid
                    container
                    spacing={2}
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    alignContent="center"
                    wrap="wrap"

                >
                    <Grid item xs={6} md={3}>
                        <Image src={pLogo1} alt='Partner 1' />
                    </Grid>
                    <Grid item xs={6} md={3}>
                        <Image src={pLogo2} alt='Partner 2' />
                    </Grid>
                    <Grid item xs={6} md={3}>
                        <Image src={pLogo3} alt='Partner 3' />
                    </Grid>
                    <Grid item xs={6} md={3}>
                        <Image src={pLogo4} alt='Partner 4' />
                    </Grid>
                </Grid>
            </Paper>
            <Box>
                <Typography
                    variant="h1"
                    sx={{
                        color: 'white',
                        fontSize: { xs: '2em', sm: '3em', md: '4em' },
                        overflow: 'hidden',
                        display: '-webkit-box',
                        WebkitBoxOrient: 'vertical',
                        WebkitLineClamp: { xs: 4, sm: 3, md: 2 },
                    }}
                >
                    LETS CHANGE YOUR OWN HOME INTERIOR DESIGN NOW
                </Typography>
                <Button
                    variant="contained"
                    color="inherit"
                    sx={{
                        mt: 3,
                        bgcolor: 'white',
                        color: 'inherit',
                        py: 2,
                        px: 4,
                    }}>
                    Get Started
                </Button>
            </Box>
        </Paper>
    );
}
