import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';
import * as React from 'react';

import styles from '/src/styles/navbar.module.css';
import Link from 'next/link';


export default function Navbar() {

    return (
        <AppBar position="static">
            <Container maxWidth="xl"
                className={styles.nav}>
                <Toolbar disableGutters>

                    <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'flex' } }}>

                        <Link href="/workShop/onrain" 
                            className={styles.link}
                            sx={{ my: 2, display: 'block'}}>
                            About
                        </Link>
                        <Link href="/workShop/onrain/workpage" 
                            className={styles.link}
                            sx={{ my: 2, display: 'block' }}>
                            เครื่องคิดเลข
                        </Link>
                        <Link href="/workShop/onrain/workshop2" 
                            className={styles.link}
                            sx={{ my: 2, display: 'block' }}>
                            workshop2
                        </Link>
                        <Link href="/workShop/onrain/workshop3" 
                            className={styles.link}
                            sx={{ my: 2, display: 'block' }}>
                            workshop3
                        </Link>
                        <Link href="/workShop/onrain/workshop4" 
                            className={styles.link}
                            sx={{ my: 2, display: 'block' }}>
                            workshop4
                        </Link>
                        <Link href="/workShop/onrain/workshop5" 
                            className={styles.link}
                            sx={{ my: 2, display: 'block' }}>
                            workshop5
                        </Link>
                        <Link href="/workShop/onrain/workshop6" 
                            className={styles.link}
                            sx={{ my: 2, display: 'block' }}>
                            workshop6
                        </Link>

                    </Box>

                </Toolbar>
            </Container>
        </AppBar>
    )
}