

export const GetData = async (api) => {
    return fetch(api, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(data => data.json())

}
export const Crud = async (api, body, method) => {
    return fetch(api, {
        method: method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
    })
        .then(data => data.json())
}

export async function User(api) {
    const accessToken = localStorage.getItem('accessToken');

    return fetch(api, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
            
        }
    })
        .then(data => data.json())
}

