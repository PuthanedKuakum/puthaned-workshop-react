
import {
    Box,
    Button,
    Link,
    TextField
} from "@mui/material";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Image from "next/image";


import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import EmailIcon from '@mui/icons-material/Email';

import styles from '/src/styles/orn/footerwork2.module.css';




const FooterWork2 = () => {

    return (
        <>
            <Box sx={{ display: 'flex', justifyContent: 'center', height: '15vh' }}>
                <Box mr={10}
                    ml={10}
                    sx={{ backgroundColor: '#FFF', transform: 'translate(0,25vh)', zIndex: '2', width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }} >
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Image src="/orn/logo-14 1.png"
                            width={150}
                            height={40}
                            className={styles.logo} />
                        <Image src="/orn/logo-16 1.png"
                            width={150}
                            height={40}
                            className={styles.logo} />
                        <Image src="/orn/logo-25 1.png"
                            width={150}
                            height={40}
                            className={styles.logo} />
                        <Image src="/orn/logo-26 1.png"
                            width={150}
                            height={40}
                            className={styles.logo} />
                    </Box>
                </Box>


            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center', transform: 'translate(0,16vh)' }}>
                <Box sx={{ backgroundColor: '#4F6F52', width: '100%' }}
                    pb={3}
                    pt={7}
                    pl={7}
                    pr={7}
                    m={3}>
                    <p style={{ fontWeight: '900', color: '#FFF', fontSize: '40px' }}>LETS CHANGE YOUR OWN HOME INTERIOR DESIGN NOW</p>
                    <Button sx={{ backgroundColor: '#3A4D39', color: '#FFF', borderRadius: '0' }}>
                        CONTACT US
                    </Button>
                </Box>
            </Box>
            <Box sx={{ backgroundColor: '#3A4D39', color: '#FFF', paddingTop: '20vh' }}>
                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}
                    pl={3}
                    pr={3}>
                    <Box sx={{ maxWidth: '450px' }}>
                        <div style={{ fontWeight: 'bold' }}>INFORMATION</div>
                        <Box mt={1}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</Box>
                    </Box>
                    <Box>
                        <div style={{ fontWeight: 'bold' }}>NAVIGATION</div>
                        <List>
                            <ListItem >
                                <ListItemIcon sx={{ minWidth: '40px' }}>
                                    <ArrowForwardIosIcon />
                                </ListItemIcon>
                                <ListItemText>
                                    <Link sx={{ textDecoration: 'none', color: '#FFF' }}
                                        href="#">Homepage</Link>
                                </ListItemText>
                            </ListItem>
                            <ListItem >
                                <ListItemIcon sx={{ minWidth: '40px' }}>
                                    <ArrowForwardIosIcon />
                                </ListItemIcon>
                                <ListItemText >
                                    <Link sx={{ textDecoration: 'none', color: '#FFF' }}
                                        href="#">About Us</Link>

                                </ListItemText>
                            </ListItem>
                            <ListItem >
                                <ListItemIcon sx={{ minWidth: '40px' }}>
                                    <ArrowForwardIosIcon />
                                </ListItemIcon>
                                <ListItemText>
                                    <Link sx={{ textDecoration: 'none', color: '#FFF' }}
                                        href="#">Services</Link>

                                </ListItemText>
                            </ListItem>
                            <ListItem >
                                <ListItemIcon sx={{ minWidth: '40px' }}>
                                    <ArrowForwardIosIcon />
                                </ListItemIcon>
                                <ListItemText >
                                    <Link sx={{ textDecoration: 'none', color: '#FFF' }}
                                        href="#">Project</Link>

                                </ListItemText>
                            </ListItem>

                        </List>

                    </Box>
                    <Box>
                        <div style={{ fontWeight: 'bold' }}>CONTACT US</div>
                        <Box sx={{ display: 'flex' }}
                            mt={1}><LocationOnIcon /><Box ml={1}>Lumbung Hidup East Java</Box></Box>
                        <Box sx={{ display: 'flex' }}
                            mt={1}><EmailIcon /><Box ml={1}>Hello@Homco.com</Box> </Box>
                        <Box
                            component="form"
                            sx={{
                                '& > :not(style)': { mb: 2, mt: 2, width: '25ch' },
                            }}
                            noValidate
                            autoComplete="off"
                        >
                            <TextField id="filled-basic"
                                label="Email Address"
                                variant="filled" />
                        </Box>
                        <Button sx={{ backgroundColor: '#4F6F52', color: '#FFF', borderRadius: '0' }}>
                            SUBSCRIBE
                        </Button>
                    </Box>

                </Box>
                <Box m={3}>
                    <hr />
                </Box>

                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}
                    m={3}>
                    <Box>ALLRIGHT RESERCED - HOMCO INTERIOR</Box>
                    <Box sx={{ display: 'flex' }}>
                        <Link href="#"
                            ml={2}
                            sx={{ color: '#FFF', textDecoration: 'none' }}>DISCLAIMER</Link>
                        <Link href="#"
                            ml={2}
                            sx={{ color: '#FFF', textDecoration: 'none' }}>PRIVACY POLICY</Link>
                        <Link href="#"
                            ml={2}
                            sx={{ color: '#FFF', textDecoration: 'none' }}>TERM OF USE</Link>
                    </Box>
                </Box>
            </Box>

        </>
    )
}

export default FooterWork2;